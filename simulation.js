const { traceWriter, TraceWriter } = require("./utils/traceWriter");

let simulationGetDict = {
  lamp: true,
  greenbean_loader: true,
  stirrer: false,
  cooler: true,
  destoner: false,
  roaster: 1,
  current_air_temperature: 20,
  current_burner_power: 10,
  current_bean_temperature: 20,
  current_drum_pressure: 30,
  set_air_temperature: 50,
  set_drum_pressure: 20,
  set_burner_power: 50,
  set_drum_speed: 70,
  set_destoner_speed: 50,
  set_stirrer_power: 50,
  set_cooler_power: 50,
  ROR: 0,
  events: [0, 0, 0, 0, 0],
  highesttemp: 0,
  CO2_reduction: 0,
  energy_consumption: 10,
  eco_score: 10,
  bean_weight: 0,
  bean_cooling: 0,
  bean_weight_green: 0,
  green_bean_loader: true,
  max_capacity: 15,
  drumpressure_min: 20,
  drumpressure_max: 100,
  drumpressure_default: 30,
  drumspeed_min: 20,
  drumspeed_max: 100,
  drumspeed_default: 50,
  roastingpower_min: 20,
  roastingpower_max: 100,
  roastingpower_default: 50,
  temperature_air_min: 30,
  temperature_air_max: 250,
  temperature_air_default: 100,
  destonerspeed_min: 20,
  destonerspeed_max: 100,
  destonerspeed_default: 40,
  stirrerspeed_min: 20,
  stirrerspeed_max: 100,
  stirrerspeed_default: 40,
  coolerspeed_min: 20,
  coolerspeed_max: 100,
  coolerspeed_default: 40,
};

const simulationPutDict = [
  "greenbean_loader",
  "set_air_temperature",
  "set_burner_power",
  "set_drum_pressure",
  "set_lamp",
  "set_stirrer",
  "set_cooler",
  "set_roaster",
  "set_abortroast",
  "set_drum_speed",
  "set_destoner_speed",
  "set_stirrer_power",
  "set_cooler_power",
  "set_destoner",
  "reset_alarm",
  "reset_alarm_long",
  "green_beans_tare",
  "set_heartbeat",
  "set_heartbeat_backend",
];

function readSymbol(symbol) {
  if (symbol == simulationGetDict.roaster) {
    if (simulationGetDict.roaster >= 4 && simulationGetDict.roaster <= 7) {
      return true;
    } else {
      return false;
    }
  }
  return simulationGetDict[symbol];
}

function writeSymbol(symbol, value) {
  switch (symbol) {
    case simulationPutDict[0]: {
      simulationGetDict.greenbean_loader = !simulationGetDict.greenbean_loader;
      return;
    }
    case simulationPutDict[1]: {
      simulationGetDict.set_air_temperature = value;
      return;
    }
    case simulationPutDict[2]: {
      simulationGetDict.set_burner_power = value;
      return;
    }
    case simulationPutDict[3]: {
      simulationGetDict.set_drum_pressure = value;
      return;
    }
    case simulationPutDict[4]: {
      simulationGetDict.lamp = !simulationGetDict.lamp;
      return;
    }
    case simulationPutDict[5]: {
      simulationGetDict.stirrer = !simulationGetDict.stirrer;
      return;
    }
    case simulationPutDict[6]: {
      simulationGetDict.cooler = !simulationGetDict.cooler;
      return;
    }
    case simulationPutDict[7]: {
      if (simulationGetDict.roaster >= 4 && simulationGetDict.roaster <= 7) {
        simulationGetDict.roaster = 1;
      } else {
        simulationGetDict.roaster = 2;
      }
      return;
    }
    case simulationPutDict[8]: {
      simulationGetDict.roaster = 2;
      return;
    }
    case simulationPutDict[9]: {
      simulationGetDict.set_drum_speed = value;
      return;
    }
    case simulationPutDict[10]: {
      simulationGetDict.set_destoner_speed = value;
      return;
    }
    case simulationPutDict[11]: {
      simulationGetDict.set_stirrer_power = value;
      return;
    }
    case simulationPutDict[12]: {
      simulationGetDict.set_cooler_power = value;
      return;
    }
    case simulationPutDict[13]: {
      simulationGetDict.destoner = !simulationGetDict.destoner;
      return;
    }
    case simulationPutDict[14]: {
      if (simulationGetDict.events.length > 0) {
        simulationGetDict.events.forEach((event) => {
          if (event.LongReset_Required == false && event.Active == true) {
            simulationGetDict.events = simulationGetDict.events.filter(
              (item) => !(item == event)
            );
          }
        });
      }
    }
    case simulationPutDict[15]:
      {
        if (simulationGetDict.events.length > 0) {
          simulationGetDict.events.forEach((event) => {
            if (event.Active == true) {
              simulationGetDict.events = simulationGetDict.events.filter(
                (item) => !(item == event)
              );
            }
          });
        }
      }
      return;

    case simulationPutDict[16]: {
      return;
    }
    default:
      return;
  }
}

class Simulation {
  constructor(set) {
    this.simulationProgramSet = set;
    this.seconds = 0;
    this.skipTick = 0;
  }

  start() {
    if (this.simulationProgramSet) {
      this.roast();
    } else {
      traceWriter.trace(
        "No valid simulation program given",
        TraceWriter.WARNING,
        TraceWriter.AREA_ALL
      );
    }
  }

  cycleRoastModes() {
    if (this.seconds % 10 == 0) {
      if (simulationGetDict.roaster == 8) {
        simulationGetDict.roaster = 1;
      }
      if (
        simulationGetDict.roaster == 4 ||
        simulationGetDict.roaster == 5 ||
        simulationGetDict.roaster == 6
      ) {
        this.skipTick++;
        if (this.skipTick == 5) {
          this.skipTick = 0;
          simulationGetDict.roaster++;
        }
      } else {
        simulationGetDict.roaster++;
      }
    }
  }

  sineBetween(min, max, t) {
    var halfRange = (max - min) / 2;
    return min + halfRange + Math.sin(t) * halfRange;
  }

  cosBetween(min, max, t) {
    var halfRange = (max - min) / 2;
    return min + halfRange + Math.cos(t) * halfRange;
  }

  roast() {
    traceWriter.trace(
      "Starting sine simulation program",
      TraceWriter.INFO,
      TraceWriter.AREA_ALL
    );

    setInterval(() => {
      this.seconds++;

      this.cycleRoastModes();

      simulationGetDict.current_air_temperature = this.sineBetween(
        100,
        240,
        this.seconds / 10
      );
      simulationGetDict.current_bean_temperature = this.cosBetween(
        10,
        50,
        this.seconds / 10
      );
      simulationGetDict.bean_weight_green = this.sineBetween(
        0,
        15,
        this.seconds / 10
      );
      simulationGetDict.current_burner_power =
        100 - this.cosBetween(20, 80, this.seconds / 10);
      simulationGetDict.bean_weight =
        15 - this.sineBetween(0, 15, this.seconds / 10);

      if (
        (this.seconds == 20 && simulationGetDict.roaster == 4) ||
        simulationGetDict.roaster == 5 ||
        simulationGetDict.roaster == 6
      ) {
        simulationGetDict.ROR = 12;
      } else {
        simulationGetDict.ROR = 2;
      }
    }, 1000);
  }
}

module.exports = {
  readSymbol,
  writeSymbol,
  Simulation,
};
