const roaster = require("./config/dmr");
const beckHoff = require("./beckhoff")(roaster);
const format = require("date-format");
const database = require("./config/mongodb").database;
const { traceWriter, TraceWriter } = require("./utils/traceWriter");

// let offsets = []
let history = [];
let second = 0;
let createHistory = true;
// let newProgram = { newProgram: false }
// let presets = {}
let currentVals = {};
let docId;
let roastInit = true;
// let startRoastTime = {}
// let roasting = {}

function equals(arrOne, arrTwo) {
  if (arrOne && arrTwo && arrTwo.length == arrOne.length) {
    for (var i = 0; i < arrOne.length; i++) {
      if (arrOne[i] != arrTwo[i]) return false;
    }
    return true;
  }
  return false;
}

exports.handlePrograms = async function (parsedData) {
  // console.log(`ARE WE ROASTING: ${(parsedData.roaster >= 4 && parsedData.roaster < 7) ? "YES" : "NO"}`)
  if (parsedData.roaster >= 4 && parsedData.roaster < 7) {
    // roasting....
    currentRoast.roasting.roasting = true;

    if (roastInit) {
      traceWriter.trace(
        `Starting a brand new roast at ${Date.now}`,
        TraceWriter.INFO,
        TraceWriter.AREA_HANDLEPROGRAMS
      );
      // Get the time of the start of the roast
      currentRoast.startRoastTime.startRoastTime = Date.now();
      currentRoast.greenBeanWeight = beanLoader.currentWeight;
      beanLoader.currentWeight = 0;
      roastInit = false;

      database.logs.insert({
        log: "Roast has started",
        createdAt: new Date(),
      });
    }
    //second += 1
    //console.log("second", second)

    // Get the presets and offsets of the program if a program is selected or add the
    // current values as presets
    if (
      Object.keys(currentRoast.presets).length === 0 &&
      Object.keys(currentRoast.offsets.length === 0)
    ) {
      traceWriter.trace(
        `NO PRESENTS OR OFFSETS IN CURRENT ROAST, SO RECORDING THE PRESETS IN CURRENT ROAST`,
        TraceWriter.INFO,
        TraceWriter.AREA_HANDLEPROGRAMS
      );

      try {
        // Get current values
        let set_drumPressure = await beckHoff.readSymbol("set_drum_pressure");
        let set_airTemperature = await beckHoff.readSymbol(
          "set_air_temperature"
        );
        let set_burnerPower = await beckHoff.readSymbol("set_burner_power");

        currentRoast.presets = {
          drum_pressure: set_drumPressure.set_drum_pressure,
          air_temperature: set_airTemperature.set_air_temperature,
          burner_power: set_burnerPower.set_burner_power,
        };

        currentVals = { ...currentRoast.presets };
      } catch (e) {
        traceWriter.trace(
          `Something went wrong when getting a setting2: ${e}`,
          TraceWriter.ERROR,
          TraceWriter.AREA_BECKHOFF
        );
      }

      // Get current program
      let program;

      // database.loadData();
      // let currentProgram = database.find("currentProgramCollection", {id: 1});

      let currentProgram = await database.currentProgramCollection.findOne({}); // .sort({ createdAt: -1 })
      traceWriter.trace(
        `CURRENT PROGRAM NOW is ${currentProgram}`,
        TraceWriter.INFO,
        TraceWriter.AREA_HANDLEPROGRAMS
      );
      if (currentProgram != null && currentProgram.currentProgram != null) {
        program = await database.programsCollection.findOne({
          id: Number(currentProgram.currentProgram),
        }); // .sort({ createdAt: -1 })
      }
      // console.log(`FOUND THE DOCUMENT IN THE PROGRAMS COLLECTION ${program}`);
      traceWriter.trace(
        `FOUND THE DOCUMENT IN THE PROGRAMS COLLECTION ${program}`,
        TraceWriter.INFO,
        TraceWriter.AREA_HANDLEPROGRAMS
      );

      // If current program is set to null by frontend a new program will be created.
      if (currentProgram === null) {
        traceWriter.trace(
          `NO CURRENT PROGRAM, SO SETTING NEWPROGRAM TO TRUE AND CLEARING THE OFFSETS..`,
          TraceWriter.WARNING,
          TraceWriter.AREA_HANDLEPROGRAMS
        );

        currentRoast.newProgram.newProgram = true;
        currentRoast.offsets = [];
        currentRoast.cracks = [];
        // todo: presets
      }

      // if(currentProgram) {
      // program = database.find("programsCollection", {id: currentProgram.currentProgram});

      // }
      // database.saveChanges()

      if (!program) {
        traceWriter.trace(
          `NO CURRENT PROGRAM, SO SETTING NEWPROGRAM TO TRUE`,
          TraceWriter.WARNING,
          TraceWriter.AREA_HANDLEPROGRAMS
        );
        currentRoast.newProgram.newProgram = true;
      } else {
        traceWriter.trace(
          `THERE IS A PROGRAM, SO GETTING THE OFFSETS OF THIS PROGRAM`,
          TraceWriter.INFO,
          TraceWriter.AREA_HANDLEPROGRAMS
        );
        // Get the offsets from program
        if (
          program.presets.drum_pressure ===
            currentRoast.presets.drum_pressure &&
          program.presets.air_temperature ===
            currentRoast.presets.air_temperature &&
          program.presets.burner_power === currentRoast.presets.burner_power
        ) {
          traceWriter.trace(
            `THERE IS A PROGRAM, SO GETTING THE OFFSETS OF THIS PROGRAM, ACTUALLY SETTING THE OFFSETS`,
            TraceWriter.INFO,
            TraceWriter.AREA_HANDLEPROGRAMS
          );

          currentRoast.offsets = program.offsets.map((offset) => {
            return { ...offset, done: false };
          });
          currentRoast.cracks = program.cracks;
        } else {
          //console.log("NEW PROGAM 2")
          currentRoast.newProgram.newProgram = true;
        }
      }
    }

    //console.log(currentRoast.presets)

    //console.log("offsets", currentRoast.offsets)

    // Write the offsets of the program to the beckhoff if the second matches the
    // current second.
    try {
      let set_drumPressure = await beckHoff.readSymbol("set_drum_pressure");
      let set_airTemperature = await beckHoff.readSymbol("set_air_temperature");
      let set_burnerPower = await beckHoff.readSymbol("set_burner_power");

      currentVals = {
        drum_pressure: set_drumPressure.set_drum_pressure,
        air_temperature: set_airTemperature.set_air_temperature,
        burner_power: set_burnerPower.set_burner_power,
      };
    } catch (e) {
      traceWriter.trace(
        `Something went wrong when getting a setting2: ${e}`,
        TraceWriter.ERROR,
        TraceWriter.AREA_BECKHOFF
      );
    }

    // console.log("THE PREVIOUS OFFSET LIST IS", currentRoast.offsets);

    currentRoast.offsets.forEach((offset) => {
      traceWriter.trace(
        `CHECKING TO SEE IF AN OFFSET IS IN REACH ${offset.second} ${
          Math.round(Date.now() - currentRoast.startRoastTime.startRoastTime) /
          1000
        }`,
        TraceWriter.VERBOSE,
        TraceWriter.AREA_HANDLEPROGRAMS
      );

      let second =
        offset.second > 5000 ? Math.round(offset.second / 1000) : offset.second;
      if (
        second ===
          Math.round(
            (Date.now() - currentRoast.startRoastTime.startRoastTime) / 1000
          ) &&
        !offset.done
      ) {
        // This removes the set before the symbol
        let val = offset.setting.split("_");
        let symbol = val[1] + "_" + val[2];

        traceWriter.trace(
          `writing symbol value ${symbol} ${offset.change}`,
          TraceWriter.VERBOSE,
          TraceWriter.AREA_HANDLEPROGRAMS
        );

        if (!isNaN(currentVals[symbol] + offset.change)) {
          beckHoff.writeSymbol(
            offset.setting,
            currentVals[symbol] + offset.change,
            ""
          );
          offset.done = true;
        }
      }
    });

    //console.log("THE OFFSET LIST IS", currentRoast.offsets);
    // console.log("Current Roast has the following cracks ", currentRoast.cracks);

    createHistory = true;
    // console.log('VOEG HISTORY RECORD TOE MET PUNTEN')
    history.push({
      ms: Date.now() - currentRoast.startRoastTime.startRoastTime,
      air: Math.round(parsedData.current_air_temperature * 100) / 100,
      bean: Math.round(parsedData.current_bean_temperature * 100) / 100,
      ror: Math.round(parsedData.ROR * 100) / 100,
    });
  } else {
    currentRoast.roasting.roasting = false;

    // Create a new program
    let currentProgram = await database.currentProgramCollection.findOne({}); // .sort({ createdAt: -1 })
    if (currentRoast.newProgram.newProgram) {
      traceWriter.trace(
        `ROASTING DONE!! SAVE NEW PROGRAM..`,
        TraceWriter.INFO,
        TraceWriter.AREA_HANDLEPROGRAMS
      );
      database.logs.insert({
        log: "Roasting done",
        createdAt: new Date(),
      });

      currentRoast.newProgram.newProgram = false;
      currentRoast.saveCracks = false; // cracks are save on new program

      //console.log("new program")
      // database.loadData();

      let ids = [];

      traceWriter.trace(
        `Getting all programs`,
        TraceWriter.VERBOSE,
        TraceWriter.AREA_HANDLEPROGRAMS
      );
      // Get all the programs
      let programsCollection = await database.programsCollection
        .find({})
        .toArray();

      traceWriter.trace(
        `Retrieved all programs`,
        TraceWriter.VERBOSE,
        TraceWriter.AREA_HANDLEPROGRAMS
      );
      // let programsCollection = database.findAllData("programsCollection")

      // Get all the id of the programs to find an id that is available for the creation of the
      // program.

      if (programsCollection !== []) {
        programsCollection.forEach(function (arrayItem) {
          ids.push(arrayItem.id);
        });

        // Find an id that is not currently used in programsCollection
        for (var i in [...Array(10000).keys()]) {
          if (!ids.includes(Number(i)) && i != 0) {
            docId = i;
            traceWriter.trace(
              `New id found: ${docId}`,
              TraceWriter.VERBOSE,
              TraceWriter.AREA_HANDLEPROGRAMS
            );
            break;
          }
        }
      } else {
        docId = 1;
      }

      ///console.log("docId test")
      //console.log(docId)

      //console.log("create program", currentRoast.presets);
      //console.log("create program", currentRoast.offsets);

      // Add the program to the db
      traceWriter.trace(
        `Adding new program under following ID: ${docId}`,
        TraceWriter.INFO,
        TraceWriter.AREA_HANDLEPROGRAMS
      );
      await database.programsCollection.insertOne({
        name: `PROGRAM_${format.asString("dd-MM-yyyy_hh:mm", new Date())}`,
        id: Number(docId),
        favorite: false,
        timestamp: Date.now(),
        cracks: currentRoast.cracks,
        offsets: currentRoast.offsets,
        presets: currentRoast.presets,
      });

      // Reset offsets
      currentRoast.offsets = [];
      currentRoast.cracks = [];

      let programs = await database.programsCollection.find({}).toArray();

      //("programs", programs)

      // Remove oldest program that is not selected as a favorite if there are more than 50 programs.
      if (programs.length >= 50) {
        programs.sort(function (a, b) {
          return new Date(a.timestamp) - new Date(b.timestamp);
        });

        for (var index in programs) {
          if (programs[index].favorite === false) {
            traceWriter.trace(
              `Deleting oldest non-favorire program: ${programs[index].id}`,
              TraceWriter.INFO,
              TraceWriter.AREA_HANDLEPROGRAMS
            );

            database.programsCollection.remove({
              id: Number(programs[index].id),
            });
            break;
          }
        }
      }

      // database.insert("programsCollection",
      // {
      //     id: docId,
      //     name: `PROGRAM_${format.asString('dd-MM-yyyy_hh:mm', new Date())}`,
      //     favorite: false,
      //     timestamp: Date.now(),
      //     offsets,
      //     presets
      // }
      // );

      // database.set("currentProgramCollection", {id: 1}, {id: 1, currentProgram: docId});
      // database.saveChanges()

      // The last progam is the currentProgram. The current program is the program that is
      // currently selected.
      // console.log(`SETTING CURRENT PROGRAM TO ${docId}`);
      // console.log(`CLEAR SOME RECORDS FIRST`);
      await database.currentProgramCollection.remove();
      // console.log(`ADD NEW ONE`);
      await database.currentProgramCollection.insert({
        currentProgram: Number(docId),
      });

      traceWriter.trace(
        `Current program set to ${docId}`,
        TraceWriter.INFO,
        TraceWriter.AREA_HANDLEPROGRAMS
      );
      // let currentProgram = await database.currentProgramCollection.findOne({}).sort({ createdAt: -1 })

      // reset offsets
      // currentRoast.offsets = []
    } else {
      if (currentRoast.saveCracks) {
        currentRoast.saveCracks = false;
        // als r cracks zijn gezet of gewijzigd, dan moeten deze opgeslagen worden in het huidige recept
        program = await database.programsCollection.findOne({
          id: Number(currentProgram.currentProgram),
        }); // .
        if (program && !equals(program.cracks, currentRoast.cracks)) {
          traceWriter.trace(
            `CRACKS needs saving: ${currentRoast.cracks}`,
            TraceWriter.INFO,
            TraceWriter.AREA_HANDLEPROGRAMS
          );

          program.cracks = currentRoast.cracks;
          var result = await database.programsCollection.replaceOne(
            { id: Number(currentProgram.currentProgram) },
            program
          );
        }
      }

      //console.log("CLEARING THE OFFSETS");
      // Reset offsets
      currentRoast.offsets = [];
      currentRoast.cracks = [];
    }

    // Create new history for program
    // console.log("IS THERE HISTORY TO SAVE?", createHistory, history.length);
    // check to see if the history is at least 75% filled with data, compared to the last run
    // get the last run length
    let prevHistories;
    let prevHistory;
    if (currentProgram != null && currentProgram.currentProgram != null) {
      prevHistories = await database.historiesCollection
        .find({ id: Number(currentProgram.currentProgram) })
        .sort({ date: -1 })
        .toArray();

      prevHistory = prevHistories[0];
    }

    let previouslength = 0;
    if (prevHistory && prevHistory.history)
      previouslength = prevHistory.history.length;

    if (
      createHistory &&
      history.length > 0 &&
      history.length > previouslength * 0.75
    ) {
      if(!beckHoff.abortingRoast){

      traceWriter.trace(
        `Saving history`,
        TraceWriter.INFO,
        TraceWriter.AREA_HANDLEPROGRAMS
      );

      try {
        createHistory = false;
        let greenBeanWeight = currentRoast.greenBeanWeight;

        try {
          currentRoast.roastedBeanWeight = await beckHoff.readSymbol(
            "bean_weight"
          );
        } catch (e) {
          traceWriter.trace(
            `Something went wrong when getting bean_weight: ${e}`,
            TraceWriter.ERROR,
            TraceWriter.AREA_BECKHOFF
          );
          database.logs.insert({
            log: `ERROR: ${e}`,
            createdAt: new Date(),
          });

          currentRoast.roastedBeanWeight = 0;
        }

        let roastedBeanWeight = currentRoast.roastedBeanWeight;

        if (!docId) {
          let currentProgram = await database.currentProgramCollection.findOne(
            {}
          );

          if (currentProgram != undefined)
            docId = currentProgram.currentProgram;
        }

        database.logs.insert({
          log: "Saving history",
          createdAt: new Date(),
        });
        //console.log("docId", docId)
        //console.log(history.length)
        // database.loadData();

        // Remove previous history if it exists
        // let prevHistory = database.find("historiesCollection", { id: docId });

        // Find previous history id.
        //let prevHistory = await database.historiesCollection.find({ id: Number(docId) })

        /// Remove previous history if it exists
        // if(prevHistory) {
        //console.log("deleting previous history")
        // console.log('OUDE HISTORY WEGGOOGEN MET DOCID: ' + docId)
        //     database.historiesCollection.remove({ id: Number(docId) })
        // }

        // Add current history
        if (history.length > 0) {
          traceWriter.trace(
            `ADDING HISTORY FOR ${docId} TO DB`,
            TraceWriter.INFO,
            TraceWriter.AREA_HANDLEPROGRAMS
          );

          await database.historiesCollection.insertOne({
            id: Number(docId),
            date: Date.now(),
            greenBeanWeight: greenBeanWeight,
            roastedBeanWeight: roastedBeanWeight,
            history: history,
            createdAt: Date.now(),
          });
        }

        // reset history and second after roast.
        // console.log('HIER GOOI IK DE HISTORY LEEG');
        history = [];
        second = 0;
        

        // count all historties for this recipy, remove the oldest if count > 10
        let data = await database.historiesCollection
          .find({ id: Number(docId) })
          .sort({ date: -1 })
          .toArray();
        traceWriter.trace(
          `Number of history documents already here: ${data.length}`,
          TraceWriter.VERBOSE,
          TraceWriter.AREA_HANDLEPROGRAMS
        );
        if (data.length > 3) {
          let item = data[data.length - 1];

          traceWriter.trace(
            `Too much history (${data.length}) removing item: ${item._id}`,
            TraceWriter.INFO,
            TraceWriter.AREA_HANDLEPROGRAMS
          );

          await database.historiesCollection.remove({ _id: item._id });
        }
        // console.log(`created new history for ${docId}`)
        currentRoast.greenBeanWeight = 0;
        currentRoast.roastedBeanWeight = 0;
      } catch (e) {
        traceWriter.trace(
          `Something went wrong when saving history: ${e}`,
          TraceWriter.ERROR,
          TraceWriter.AREA_HANDLEPROGRAMS
        );
        database.logs.insert({
          log: `ERROR: ${e}`,
          createdAt: new Date(),
        });
        // console.log('uploading to db failed')
      }

      // Get the energyConsumption
      let energyConsumptionArray =
        await database.energyConsumptionCollection.findOne({ _id: 1 });

      //console.log("energyConsumption 1", energyConsumptionArray);

      if (!energyConsumptionArray) {
        energyConsumptionArray = [];
      } else {
        energyConsumptionArray = energyConsumptionArray.energyConsumption;
      }

      let energy_consumption_current;

      // Read the energy_consumption of the last roast.
      try {
        energy_consumption_current = await beckHoff.readSymbol(
          "energy_consumption",
          {}
        );
      } catch (e) {
        traceWriter.trace(
          `Reading failed`,
          TraceWriter.ERROR,
          TraceWriter.AREA_HANDLEPROGRAMS
        );
        database.logs.insert({
          log: `ERROR: ${e.message}`,
          createdAt: new Date(),
        });
      }

      // Add the energy_consumption of the last roast
      if (energy_consumption_current)
        energyConsumptionArray.push(
          energy_consumption_current.energy_consumption
        );

      // console.log("energyConsumption 3", energyConsumptionArray)

      // Only save the last 6 items of the energyconsumption
      if (energyConsumptionArray.length > 7) {
        energyConsumptionArray.shift();
      }

      // Remove previous collection
      await database.energyConsumptionCollection.remove();

      // Insert the new array in the db.
      await database.energyConsumptionCollection.insert({
        _id: 1,
        energyConsumption: energyConsumptionArray,
      });

      traceWriter.trace(
        `Added energyConsumption`,
        TraceWriter.INFO,
        TraceWriter.AREA_HANDLEPROGRAMS
      );
      } else {
        beckHoff.abortingRoast = false;
        history = [];
        second = 0;
      }
    } else {
      //traceWriter.trace(`MISLUKTE ROAST, niet opslaan!`, TraceWriter.WARNING, TraceWriter.AREA_HANDLEPROGRAMS);
    }

    if (createHistory && history.length > 0) {
      history = [];
      second = 0;
      currentRoast.greenBeanWeight = 0;
      currentRoast.roastedBeanWeight = 0;
      createHistory = false;
    }

    // Reset the presets for the next roast
    if (Object.keys(currentRoast.presets).length > 0) {
      for (let key in currentRoast.presets) {
        traceWriter.trace(
          `SETTING THE PRESETS ${currentRoast.presets}`,
          TraceWriter.INFO,
          TraceWriter.AREA_HANDLEPROGRAMS
        );

        beckHoff.writeSymbol("set_" + key, currentRoast.presets[key]);
      }

      second = 0;
      currentRoast.presets = {};
    }
    roastInit = true;
  }
};

let currentRoast = {
  greenBeanWeight: 0,
  roastedBeanWeight: 0,
  cracks: [],
  presets: [],
  offsets: [],
  startRoastTime: {},
  saveCracks: false,
  newProgram: { newProgram: false },
  roasting: {},
};

let beanLoader = {
  currentWeight: 0,
};

module.exports.currentRoast = currentRoast;
module.exports.beanLoader = beanLoader;
module.exports.history = () => {
  return history;
};
