const dgram = require("dgram");
const { networkInterfaces } = require("os");
const nets = networkInterfaces();
const { traceWriter, TraceWriter } = require("./utils/traceWriter");
const database = require("./config/mongodb").database;

class UdpBroadcaster {
  init() {
    traceWriter.trace(
      "Starting to broadcast udp",
      TraceWriter.INFO,
      TraceWriter.AREA_UDP
    );

    this.results = {};
    for (const name of Object.keys(nets)) {
      for (const net of nets[name]) {
        // skip over non-ipv4 and internal (i.e. 127.0.0.1) addresses
        if (net.family === "IPv4" && !net.internal) {
          if (!this.results[name]) this.results[name] = [];
          this.results[name].push(net.address);
        }
      }
    }
    this.start();
  }

  start() {
    let roasterUdpPackage = this.results[Object.keys(this.results)[0]][0];
    this.client = dgram.createSocket("udp4");
    this.client.on("message", (message, rinfo) => {
      traceWriter.trace(
        `udp message: ${message}`,
        TraceWriter.INFO,
        TraceWriter.AREA_UDP
      );

      if (message == "12345") {
        traceWriter.trace(
          `correct pwd, send the ipaddress of this machine: ${JSON.stringify(
            roasterUdpPackage
          )}`,
          TraceWriter.INFO,
          TraceWriter.AREA_UDP
        );

        this.sendAddress(rinfo, roasterUdpPackage);
      }
    });
    this.client.bind(41234);
    this.started = true;
  }

  stop() {
    this.started = false;

    traceWriter.trace(
      `Client connected, stop broadcasting`,
      TraceWriter.INFO,
      TraceWriter.AREA_UDP
    );

    try {
      this.client.close(() => {
        traceWriter.trace(
          `Client closed! broadcasting stopped!`,
          TraceWriter.INFO,
          TraceWriter.AREA_UDP
        );
      });
    } catch (e) {
      traceWriter.trace(
        `Already closed!`,
        TraceWriter.INFO,
        TraceWriter.AREA_UDP
      );
      database.logs.insert({
        log: `ERROR: ${e.message}`,
        createdAt: new Date(),
      });
    }
  }

  sendAddress(rinfo, roasterUdpPackage) {
    if (!this.answering) {
      this.answering = true;
      let timer;
      timer = setInterval(() => {
        if (this.started) {
          traceWriter.trace(
            `Client sending address`,
            TraceWriter.INFO,
            TraceWriter.AREA_UDP
          );

          this.client.send(
            Buffer.from(JSON.stringify({ roaster: roasterUdpPackage })),
            41234,
            rinfo.address,
            (err) => {}
          );
        }
      }, 1000);
      setTimeout(() => {
        traceWriter.trace(
          `Stopping sending adres`,
          TraceWriter.WARNING,
          TraceWriter.AREA_UDP
        );

        clearInterval(timer);
        this.answering = false;
      }, 5000);
    }
  }
}

let udpBroadCasterInstance = null;

module.exports = () => {
  if (!udpBroadCasterInstance) {
    udpBroadCasterInstance = new UdpBroadcaster();
    udpBroadCasterInstance.init();
  }
  return udpBroadCasterInstance;
};
