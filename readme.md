# DMR BACKEND

## CONFIGURATION
Make sure you edit `config/dmr.js` accordingly before running.

## AUTOSTART
Create and edit the service file:

`sudo nano lib/systemd/system/dmr.service`

Copy and paste the following inside the above file:

```
[Unit]
Description=dmr server  
After=network.target
[Service]
Environment=NODE_PORT=8080
Environment=NODE_PORT=3000
Type=simple
User=root
WorkingDirectory=/usr/src/app
ExecStart=/bin/bash autostart.sh
Restart=on-failure
[Install]
WantedBy=multi-user.target
```
Use keyboard shortcut `Ctrl + O` followed with `Ctrl + X` to save and quit

Then run the following command to enable the service on startup:

`sudo systemctl enable dmr.service`
