class TraceWriter {
  constructor(mode, category, area) {
    this.area = area;
    this.category = category;

    if (!mode) this.mode = TraceWriter.DISABLED;
    else this.mode = mode;

    if (this.mode == TraceWriter.ENABLED) process.stderr.write("\x1B[?25l");
    this.trace("Starting the trace...");
  }

  traceInline(
    line,
    category = TraceWriter.INFO,
    area = TraceWriter.AREA_GENERAL
  ) {
    // if (this.mode == TraceWriter.ENABLED) {
    //   process.stdout.write("\r");
    //   if (category <= this.category) {
    //     if (area & this.area) {
    //       switch (category) {
    //         case TraceWriter.ERROR: {
    //           process.stdout.write(
    //             `\x1b[31m\x1b[1m ◉  TRACE: !!ERROR!! ${line.toUpperCase()}\x1b[0m`
    //           );
    //           break;
    //         }
    //         case TraceWriter.WARNING: {
    //           process.stdout.write(
    //             `\x1b[33m\x1b[1m ◉  TRACE: !WARNING! ${line.toUpperCase()}\x1b[0m`
    //           );
    //           break;
    //         }
    //         case TraceWriter.INFO: {
    //           process.stdout.write(
    //             `\x1b[32m\x1b[1m ◉  TRACE: ${line.toUpperCase()}\x1b[0m`
    //           );
    //           break;
    //         }
    //         case TraceWriter.VERBOSE: {
    //           console.log(
    //             `\x1b[32m\x1b[1m ◉  TRACE: ${line.toUpperCase()}\x1b[0m`
    //           );
    //           break;
    //         }
    //       }
    //     }
    //   }
    // }
  }

  trace(line, category = TraceWriter.INFO, area = TraceWriter.AREA_GENERAL) {
    if (this.mode == TraceWriter.ENABLED) {
      if (category <= this.category) {
        if (area & this.area) {
          switch (category) {
            case TraceWriter.ERROR: {
              console.log(
                `\x1b[31m\x1b[1m ◉  TRACE: !!ERROR!! ${line.toUpperCase()}\x1b[0m`
              );
              break;
            }
            case TraceWriter.WARNING: {
              console.log(
                `\x1b[33m\x1b[1m ◉  TRACE: !WARNING! ${line.toUpperCase()}\x1b[0m`
              );
              break;
            }
            case TraceWriter.INFO: {
              console.log(
                `\x1b[32m\x1b[1m ◉  TRACE: ${line.toUpperCase()}\x1b[0m`
              );
              break;
            }
            case TraceWriter.VERBOSE: {
              console.log(
                `\x1b[32m\x1b[1m ◉  TRACE: ${line.toUpperCase()}\x1b[0m`
              );
              break;
            }
          }
        }
      }
    }
  }
}

TraceWriter.ENABLED = 1;
TraceWriter.DISABLED = 2;

TraceWriter.AREA_WEBSOCKET = 1;
TraceWriter.AREA_WEBAPI = 2;
TraceWriter.AREA_IPAD = TraceWriter.AREA_WEBSOCKET | TraceWriter.AREA_WEBAPI;
TraceWriter.AREA_HANDLEPROGRAMS = 4;
TraceWriter.AREA_BECKHOFF = 8;
TraceWriter.AREA_MONGODB = 16;
TraceWriter.AREA_NODEJS =
  TraceWriter.AREA_HANDLEPROGRAMS |
  TraceWriter.AREA_BECKHOFF |
  TraceWriter.AREA_MONGODB;
TraceWriter.AREA_UDP = 32;
TraceWriter.AREA_ALL = TraceWriter.AREA_IPAD | TraceWriter.AREA_NODEJS;

TraceWriter.VERBOSE = 50;
TraceWriter.INFO = 10;
TraceWriter.WARNING = 5;
TraceWriter.ERROR = 1;

let mode = TraceWriter.DISABLED;
let category = TraceWriter.INFO;
let area = TraceWriter.AREA_ALL;

var myArgs = process.argv.slice(2);
if (myArgs.find((i) => i == "--info")) {
  mode = TraceWriter.ENABLED;
  category = TraceWriter.INFO;
}

if (myArgs.find((i) => i == "--error")) {
  mode = TraceWriter.ENABLED;
  category = TraceWriter.ERROR;
}

if (myArgs.find((i) => i == "--warning")) {
  mode = TraceWriter.ENABLED;
  category = TraceWriter.WARNING;
}

if (myArgs.find((i) => i == "--verbose")) {
  mode = TraceWriter.ENABLED;
  category = TraceWriter.VERBOSE;
}

if (myArgs.find((i) => i == "--websocket")) {
  mode = TraceWriter.ENABLED;
  area = TraceWriter.AREA_WEBSOCKET;
}
if (myArgs.find((i) => i == "--webapi")) {
  mode = TraceWriter.ENABLED;
  area = TraceWriter.AREA_WEBAPI;
}
if (myArgs.find((i) => i == "--ipad")) {
  mode = TraceWriter.ENABLED;
  area = TraceWriter.AREA_IPAD;
}
if (myArgs.find((i) => i == "--handleprograms")) {
  mode = TraceWriter.ENABLED;
  area = TraceWriter.AREA_HANDLEPROGRAMS;
}
if (myArgs.find((i) => i == "--beckhoff")) {
  mode = TraceWriter.ENABLED;
  area = TraceWriter.AREA_BECKHOFF;
}
if (myArgs.find((i) => i == "--mongodb")) {
  mode = TraceWriter.ENABLED;
  area = TraceWriter.AREA_MONGODB;
}
if (myArgs.find((i) => i == "--nodejs")) {
  mode = TraceWriter.ENABLED;
  area = TraceWriter.AREA_NODEJS;
}
if (myArgs.find((i) => i == "-all")) {
  mode = TraceWriter.ENABLED;
  area = TraceWriter.AREA_ALL;
}

var traceWriter = traceWriter || new TraceWriter(mode, category, area);

module.exports = {
  traceWriter: traceWriter,
  TraceWriter: TraceWriter,
};
