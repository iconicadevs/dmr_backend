const express = require("express");
const { getConfiguration, getGreenBeanInfo } = require("../controllers/configurations");
const { getVersion } = require("../controllers/version");

const router = express.Router();

router.route("/").get(getConfiguration);

router.route("/version").get(getVersion);

router.route("/greenBean").get(getGreenBeanInfo)

module.exports = router;
