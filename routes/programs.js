const express = require('express');
const asyncHandler = require('../middleware/async')

const { 
    getProgram,
    getAllPrograms,
    renameProgram,
    setProgram,
    currentProgram,
    setCrack,
    getProgress,
} = require('../controllers/programs');

const router = express.Router();

router.route('/getProgress').get(getProgress)

router
    .route('/currentProgram')
    .get(currentProgram)

router
    .route('/currentProgram/set_crack1')
    .post(asyncHandler(async (req, res, next) => { setCrack(1, req.body.val);res.status(200).json({ success: true }); } ))

router
    .route('/currentProgram/set_crack2')
    .post(asyncHandler(async (req, res, next) => { setCrack(2, req.body.val);res.status(200).json({ success: true }); } ))

router
    .route('/:program')
    .get(getProgram)
    .post(renameProgram)

router
    .route('/setprogram/:program')
    .post(setProgram)


router
    .route('/')
    .get(getAllPrograms)


module.exports = router;