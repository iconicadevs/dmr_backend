const express = require('express');
const { 
    getAllData,
    getSymbol,
    updateSymbol,
    undoSymbol
} = require('../controllers/symbols');

const router = express.Router();

router
    .route('/')
    .get(getAllData)

router
    .route('/undo/:symbol')
    .put(undoSymbol)

router
    .route('/:symbol')
    .get(getSymbol)
    .put(updateSymbol)

module.exports = router;