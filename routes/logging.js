const express = require("express");

const { getLogging } = require("../controllers/logging");

const router = express.Router();

router.route("/").get(getLogging);

module.exports = router;
