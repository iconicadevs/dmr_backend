const express = require('express');
const { 
    sendHeartbeat,
} = require('../controllers/heartbeat');

const router = express.Router();

router
    .route('/')
    .post(sendHeartbeat)

module.exports = router;