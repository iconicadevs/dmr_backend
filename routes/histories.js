const express = require('express');
const { 
    getMostRecentHistory,
    john
} = require('../controllers/histories');

const router = express.Router();


router
    .route('/mostRecentHistory/:program')
    .get(getMostRecentHistory)

router
    .route('/john')
    .get(john)

module.exports = router;