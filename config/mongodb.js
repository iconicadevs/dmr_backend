var MongoClient = require("mongodb").MongoClient;
const { traceWriter, TraceWriter } = require("../utils/traceWriter");

var database = {};

function connect() {
  return new Promise((res, rej) => {
    MongoClient.connect("mongodb://localhost:27017/", function (err, db) {
      if (err) throw err;
      database.db = db.db("dmr");

      database.programsCollection = database.db.collection("programs");
      database.historiesCollection = database.db.collection("histories");
      database.currentProgramCollection =
        database.db.collection("currentProgram");
      database.energyConsumptionCollection =
        database.db.collection("energyConsumption");
      database.logs = database.db.collection("logs");

      database.logs.createIndex(
        { createdAt: 1 },
        { expireAfterSeconds: 172800 }
      );

      traceWriter.trace(
        `Connected to database`,
        TraceWriter.INFO,
        TraceWriter.AREA_MONGODB
      );
      res();
    });
  });
}

connect();

module.exports = {
  database,
};
