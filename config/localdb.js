const fs = require('fs');
var zlib = require('zlib');

var database = {
    stats(collection) {
        if (!Array.isArray(this[collection])) throw("UNKNOWN COLLECTION");
        console.log("collection: ", collection); 
        console.log("number of records: ", this[collection].length); 
    },
    dump(collection) {
        console.log(JSON.stringify(this[collection]));
    },
    find(collection, query) {
        if (!Array.isArray(this[collection])) return;
        return this[collection].find(q => {
            for(let k of Object.keys(query)) {
                if (q[k] !== query[k]) return false;
            }
            return true;
        })
    },
    findAllData(collection) {
        if (!Array.isArray(this[collection])) return []
        return this[collection]
    },
    insert(collection, data) {
        if (!Array.isArray(this[collection])) this[collection] = [];
        if (!data.id) throw("NO ID PROVIDED", data);
        if (this.find(collection, {id:data.id})) throw("DUPLICATE KEY!");
        this[collection].push(data); 
    },
    replace(collection, query, data) {
        if (!Array.isArray(this[collection])) throw("UNKNOWN COLLECTION");
        if (!query.id) throw("NO ID PROVIDED");
        let object = this.find(collection, query);

        let newData = {...object} //Object.assign(object, data)

        console.log("new data")
        console.log(newData)

        console.log("data", data)


        for(var key in data)
            newData[key] = data[key]


        console.log("data")
        console.log(newData)
        
        if (object) { 
            this[collection].splice(this[collection].indexOf(object), 1, newData);
        }
    },
    set(collection, query, data) {
        if (!Array.isArray(this[collection])) this[collection] = [];
        if (!data.id) throw("NO ID PROVIDED", data);
        data.id = query.id;
        let object = this.find(collection, query);
        if (object) { 
            this[collection].splice(this[collection].indexOf(object), 1, data);
        } else {
            this[collection].push(data); 
        }
    },
    delete(collection, query) {
        if (!Array.isArray(this[collection])) throw("UNKNOWN COLLECTION");
        if (!query.id) throw("NO ID PROVIDED");
        let object = this.find(collection, query);
        if (object) { 
            this[collection].splice(this[collection].indexOf(object), 1);
        }
    },
    loadData() {
        if (!fs.existsSync("./data/db.log")) fs.writeFileSync("./data/db.log", "");
        const dataString = fs.readFileSync("./data/db.log", "utf8")
        let data = {};
        if (dataString.length > 0) 
            data = JSON.parse(zlib.inflateSync(Buffer.from(dataString, 'base64')).toString());
        for (let k of Object.keys(data))
            this[k] = data[k] || [];
    },
    saveChanges() {
        let data = {};
        for (let k of Object.keys(this)) {
            if (Array.isArray(this[k])) {
                data[k] = this[k];
            }
        }
        fs.writeFileSync("./data/db.log", zlib.deflateSync(JSON.stringify(data)).toString('base64'));
    }
};


module.exports = {
    database
}