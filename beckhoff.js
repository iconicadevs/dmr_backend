const WebSocket = require("ws");
const client = require("./config/dmr");
const {
  readSymbol: simRead,
  writeSymbol: simWrite,
  Simulation,
} = require("./simulation");
const { traceWriter, TraceWriter } = require("./utils/traceWriter");
const { symbolDictGet, symbolDictPut } = require("./utils/symbolDicts");
const database = require("./config/mongodb").database;

let simulationMode = false;

class WebSocketListener {
  constructor(ws, name) {
    this.name = name;
    this.ws = ws;
  }
  invoke(data) {
    traceWriter.trace(
      `Socket state: ${this.ws.readyState}`,
      TraceWriter.VERBOSE,
      TraceWriter.AREA_WEBSOCKET
    );
    if (this.ws.readyState === WebSocket.OPEN) {
      traceWriter.trace(
        `Sending data to socket`,
        TraceWriter.VERBOSE,
        TraceWriter.AREA_WEBSOCKET
      );
      this.ws.send(data);
    }
  }
}

class FunctionListener {
  constructor(func, name) {
    this.name = name;
    this.func = func;
  }
  invoke(data) {
    this.func(data);
  }
}

// const mapper = {
//     default: function (dict, name) {
//         return (val) => { dict[name] = val.value; dict.notifyListeners(); }
//     }
// }

class BeckHoff {
  constructor(roaster) {
    traceWriter.trace(
      "Creating BeckHoff PROXY",
      TraceWriter.INFO,
      TraceWriter.AREA_BECKHOFF
    );
    this.connected = false;
    this.roaster = roaster;
    this.roaster.on("connectionLost", (_) => {
      traceWriter.trace(
        "BECKHOFF connection lost",
        TraceWriter.WARNING,
        TraceWriter.AREA_BECKHOFF
      );
      this.connected = false;
    });
    this.roaster.on("disconnect", (_) => {
      traceWriter.trace(
        "BECKHOFF disconnected",
        TraceWriter.WARNING,
        TraceWriter.AREA_BECKHOFF
      );
      this.connected = false;
    });
    this.roaster.on("connect", (connectionInfo) => {
      traceWriter.trace(
        "BECKHOFF connected",
        TraceWriter.INFO,
        TraceWriter.AREA_BECKHOFF
      );
    });
    this.roaster.on("reconnect", (_) => {
      traceWriter.trace(
        "BECKHOFF connection restored",
        TraceWriter.INFO,
        TraceWriter.AREA_BECKHOFF
      );
      this.connected = true;
      this.start();
    });
    this.heartbeat = 0;
    this.listeners = [];
    this.timeout = -1;
    this.i = 0;
    this.history = [];
    this.bean_temp_history = [];
    this.air_temp_history = [];
    // The object that will be sent to the frontend in the websocket
    this.dataObj = { histories: [] };
    this.ROR_history = [];
    this.subscriptions = [];
    this.roastInit = true;
    this.abortingRoast = false;
    this.symbols = {
      current_bean_temperature: "GVL_IO.iTemperature_BeanDrum.Value",
      greenbean_loader: "GVL_Status.GreenBeanLoader_IsOn",
      current_air_temperature: "GVL_IO.iTemperature_Air.Value",
      roaster: "GVL_Status.RoasterMode.nCurrentMode",
      lamp: "GVL_Status.SpotLight_IsOn",
      stirrer: "GVL_Status.Stirrer_IsOn",
      cooler: "GVL_Status.Cooler_IsOn",
      destoner: "GVL_Status.Destoner_IsOn",
      highesttemp: "GVL_Status.HighestTemperature",
      set_burner_power: "GVL_Settings.RoastingPower.Value",
      current_burner_power: "GVL_Status.RoasterPower_Perc",
      current_drum_pressure: "GVL_IO.iDrumPressure.Value",
      set_air_temperature: "GVL_Settings.Temperature_Air.Value",
      set_drum_pressure: "GVL_Settings.DrumPressure.Value",
      set_drum_speed: "GVL_Settings.DrumpSpeed.Value",
      set_destoner_speed: "GVL_Settings.Destoner_Speed.Value",
      set_stirrer_power: "GVL_Settings.Stirrer_Speed.Value",
      set_cooler_power: "GVL_Settings.Cooler_Speed.Value",
      ROR: "GVL_IO.iROR_Bean.value",
      events: "GVL_EventsCopy.events",
      eco_score: "GVL_Status.EcoReport_Score",
      bean_weight: "GVL_IO.iWeight_Beans_Roasted.Value",
      bean_cooling: "GVL_Status.BeanCooling_Remain_s",
      bean_weight_green: "GVL_IO.iWeight_Beans_Green.Value",
      auto_roast: "GVL_Settings.AutoRoastStart_Ena",
      roast_duration: "GVL_StatusRetained.CurrentRoast.Duration_s",
    };
  }

  registerWebSocketListener(ws, name) {
    traceWriter.trace(
      "Websocket client subscribed to beckhoff for data: " + name,
      TraceWriter.INFO,
      TraceWriter.AREA_WEBSOCKET
    );
    this.listeners.push(new WebSocketListener(ws, name));
  }
  registerFunctionListener(func, name) {
    traceWriter.trace(
      "Function client subscribed to beckhoff for data: " + name,
      TraceWriter.INFO,
      TraceWriter.AREA_WEBSOCKET
    );
    this.listeners.push(new FunctionListener(func, name));
  }

  registerListener(listener) {
    this.listeners.push(listener);
  }

  unregisterListener(listener) {
    var target = this.listeners.find((l) => l.ws === listener);
    if (target) {
      traceWriter.trace(
        "Client unsubscribed to beckhoff for data: " + target.name,
        TraceWriter.INFO,
        TraceWriter.AREA_BECKHOFF
      );
      this.listeners.splice(this.listeners.indexOf(target), 1);
    }
  }

  async connect() {
    try {
      traceWriter.trace(
        "Connecting to the beckhoff",
        TraceWriter.INFO,
        TraceWriter.AREA_BECKHOFF
      );
      const res = await this.roaster.connect();
      this.connected = true;
      if (this.connectinterval) clearInterval(this.connectinterval);
      this.connectinterval = null;
      this.start(res);
    } catch (err) {
      traceWriter.trace(
        err.message,
        TraceWriter.ERROR,
        TraceWriter.AREA_BECKHOFF
      );
      database.logs.insert({
        log: `ERROR: ${err.message}`,
        createdAt: new Date(),
      });
    }
  }

  async init() {
    this.connected = false;
    this.listenersNotified = true;

    var myArgs = process.argv.slice(2);
    let set = false;
    if (myArgs.find((i) => i == "--mock")) {
      simulationMode = true;
      this.start(simulationMode);
      let simulator;
      myArgs.find((i) => {
        if (i == "sine" && set == false) {
          simulator = new Simulation(true);
          set = true;
        }
      });
      if (simulator == undefined) {
        simulator = new Simulation(false);
      }
      simulator.start();
    }

    if (simulationMode) {
      this.connected = true;
    } else {
      await this.connect();
      if (!this.connected)
        this.connectinterval = setInterval(this.connect.bind(this), 5000);
    }
  }

  async start(res) {
    if (!typeof res === Boolean) {
      traceWriter.trace(
        `Connected, Router assigned us AmsNetId ${res.localAmsNetId} and port ${res.localAdsPort}`,
        TraceWriter.INFO,
        TraceWriter.AREA_BECKHOFF
      );
    }
    // Get the data from the beckhoff
    this.readinterval = setInterval(async () => {
      try {
        if (!this.connected) {
          clearInterval(this.readinterval);
          return;
        }

        if (this.listenersNotified) {
          this.listenersNotified = false;

          traceWriter.trace(
            `GETTING DATA`,
            TraceWriter.VERBOSE,
            TraceWriter.AREA_BECKHOFF
          );

          let promises = [];
          let self = this;

          self
            .writeSymbol("set_heartbeat_backend", this.heartbeat)
            .then((_) => {
              this.heartbeat = (this.heartbeat + 1) % 1000;
            })
            .catch((err) => {
              traceWriter.trace(
                err.message,
                TraceWriter.ERROR,
                TraceWriter.AREA_BECKHOFF
              ),
                database.logs.insert({
                  log: `ERROR: ${err.message}`,
                  createdAt: new Date(),
                });
            });

          // Get all all data
          Object.keys(this.symbols).forEach(function (key) {
            promises.push(self.readSymbol(key, {}));
          });

          Promise.all(promises)
            .then((data) => {
              this.beckhoffDataObj = {};
              // Add all values to objects to send to user
              data.forEach((d) => {
                if (d != undefined)
                  //console.log("D IS " + JSON.stringify(d));
                  this.beckhoffDataObj[Object.keys(d)[0]] = Object.values(d)[0];
                this.dataObj[Object.keys(d)[0]] = Object.values(d)[0];
              });
              // if(this.beckhoffDataObj.roaster != undefined)
              // Add only int (programstatus) and remove String
              if (simulationMode) {
                this.dataObj.roaster = this.beckhoffDataObj.roaster;
                this.beckhoffDataObj.roaster = this.beckhoffDataObj.roaster;
              } else {
                this.dataObj.roaster = this.beckhoffDataObj.roaster.value;
                this.beckhoffDataObj.roaster =
                  this.beckhoffDataObj.roaster.value;
              }
              // console.log("NOTIFY LISTENERS")
              this.notifyListeners();
              this.listenersNotified = true;
            })
            .catch((err) => {
              traceWriter.trace(
                err.message,
                TraceWriter.ERROR,
                TraceWriter.AREA_BECKHOFF
              );
              database.logs.insert({
                log: `ERROR: ${err.message}`,
                createdAt: new Date(),
              });
              this.listenersNotified = true;
            });
        } else {
          // console.log("WAITING FOR DATA ")
        }
      } catch (err) {
        traceWriter.trace(
          err.message,
          TraceWriter.ERROR,
          TraceWriter.AREA_BECKHOFF
        ); // some coding error in handling happened
        database.logs.insert({
          log: `ERROR: ${err.message}`,
          createdAt: new Date(),
        });
      }
    }, 100);
  }

  throttle(func, interval) {
    if (this.timeout == -1) {
      this.timeout = setTimeout(() => (this.timeout = -1), interval);
      func();
    }
  }

  notifyListeners() {
    this.throttle(() => {
      for (let listener of this.listeners) {
        traceWriter.trace(
          `Sending data to listeners ${this.listeners}`,
          TraceWriter.VERBOSE,
          TraceWriter.AREA_BECKHOFF
        );
        let events = [];

        // Only add active events to websocket
        this.beckhoffDataObj.events.forEach((event) => {
          if (event.Active) {
            //console.log("********* timestamp from beckhoff ", event.TimeStamp_Raised);
            //console.log("********* timestamp new ", Number(event.TimeStamp_Raised));
            //console.log(process.version)
            //console.log("********* timestamp new (2) ", Number(event.TimeStamp_Raised));
            //console.log("********* timestamp new (2) ", Buffer.from(event.TimeStamp_Raised).readInt32LE(4) + "" + Buffer.from(event.TimeStamp_Raised).readInt32LE(0));
            events.push(
              JSON.parse(
                JSON.stringify({
                  Active: event.Active,
                  LongReset_Required: event.LongReset_Required,
                  TimeStamp_Raised: Number(event.TimeStamp_Raised),
                  Description: event.Description,
                })
              )
            );
          }
        });

        // Add active events to dataObject
        this.dataObj.events = events;

        //this.dataObj["set_burner_power"] = this.beckhoffDataObj["current_burner_power"]

        // If currently roasting
        if (
          this.beckhoffDataObj.roaster >= 4 &&
          this.beckhoffDataObj.roaster <= 7
        ) {
          if (this.roastInit) {
            this.startRoastTime = Date.now();
            this.roastInit = false;
            this.dataObj["histories"] = [];
          }

          let historyObj = {};

          // Add history to object and add object to list histories
          if (
            this.beckhoffDataObj.current_bean_temperature != undefined &&
            (this.beckhoffDataObj.current_air_temperature != undefined) &
              (this.beckhoffDataObj.ROR != undefined)
          ) {
            historyObj.bean =
              Math.round(this.beckhoffDataObj.current_bean_temperature * 100) /
              100;
            historyObj.air =
              Math.round(this.beckhoffDataObj.current_air_temperature * 100) /
              100;
            historyObj.ror = Math.round(this.beckhoffDataObj.ROR * 100) / 100;
            historyObj.ms = Date.now() - this.startRoastTime;
            this.dataObj["histories"].push(historyObj);
          }

          traceWriter.traceInline(
            `Sending data to subscriber: ${listener.name} ${
              ["|", "\\", "-", "/"][this.i++ % 4]
            }`,
            TraceWriter.INFO,
            TraceWriter.AREA_BECKHOFF
          );
          traceWriter.trace(
            `data: ${JSON.stringify(this.dataObj)}`,
            TraceWriter.VERBOSE,
            TraceWriter.AREA_BECKHOFF
          );
          
          listener.invoke(JSON.stringify(this.dataObj));
          this.dataObj["histories"] = [];
        } else {
          // Not currently roasting
          this.roastInit = true;
          // Reset history if not roasting
          this.dataObj["histories"] = [];

          //  If beckhoff is not roasting only add current point as history
          if (
            this.beckhoffDataObj.current_air_temperature &&
            this.beckhoffDataObj.current_bean_temperature &&
            this.beckhoffDataObj.ROR
          ) {
            this.bean_temp_history = [
              Math.round(this.beckhoffDataObj.current_bean_temperature * 100) /
                100,
            ];
            this.air_temp_history = [
              Math.round(this.beckhoffDataObj.current_air_temperature * 100) /
                100,
            ];
            this.ROR_history = [
              Math.round(this.beckhoffDataObj.ROR * 100) / 100,
            ];
          }

          this.dataObj["bean"] = this.bean_temp_history;
          this.dataObj["air"] = this.air_temp_history;
          this.dataObj["ror"] = this.ROR_history;

          // console.log("DATA FOR " + listener.name + " LETS BRING IT ON!!");
          traceWriter.traceInline(
            `Sending data to subscriber: ${listener.name} ${
              ["|", "\\", "-", "/"][this.i++ % 4]
            }`,
            TraceWriter.INFO,
            TraceWriter.AREA_BECKHOFF
          );
          traceWriter.trace(
            `data: ${JSON.stringify(this.dataObj)}`,
            TraceWriter.VERBOSE,
            TraceWriter.AREA_BECKHOFF
          );
          listener.invoke(JSON.stringify(this.dataObj));

          // Reset all history lists
          this.bean_temp_history = [];
          this.air_temp_history = [];
          this.ROR_history = [];
        }
      }
    }, 100);
  }

  async writeSymbol(symbol, val, res) {
    if (!this.connected) {
      return {};
    }
    let data = {};
    if (simulationMode) {
      simWrite(symbol, val);
      if (typeof result === "number")
        data[symbolDictPut[symbol]] = Math.round(val * 100) / 100;
      else data[symbolDictPut[symbol]] = val;
    } else {
      if (
        symbol != "set_air_temperature" &&
        symbol != "set_burner_power" &&
        symbol != "set_drum_pressure" &&
        symbol != "set_stirrer" &&
        symbol != "set_cooler" &&
        symbol != "set_lamp" &&
        symbol != "set_heartbeat" &&
        symbol != "set_heartbeat_backend" &&
        symbol != "reset_alarm" &&
        symbol != "reset_alarm_long" &&
        symbol != "set_destoner" &&
        symbol != "set_abortroast" &&
        symbol != "set_startroast" &&
        symbol != "set_stoproast" &&
        symbol != "set_roaster" &&
        symbol != "greenbean_loader" &&
        symbol != "green_beans_tare" && 
        symbol !=  "set_auto_roast"
      ) {
        res
          .status(404)
          .json({ success: false, message: "This type does not exist" });
      }

      if (
        symbol === "set_air_temperature" ||
        symbol === "set_burner_power" ||
        symbol === "set_drum_pressure" ||
        symbol === "set_heartbeat" ||
        symbol === "set_heartbeat_backend" ||
        symbol === "set_cooler_power" ||
        symbol === "set_stirrer_power" ||
        symbol === "set_destoner_speed" ||
        symbol === "set_drum_speed"
      ) {
        if (typeof val === "number") {
          Object.keys(symbolDictPut).forEach(function (key) {
            if (key == symbol) {
              traceWriter.trace(
                `Writing Symbol ${symbolDictPut[key]} : ${val}`,
                TraceWriter.VERBOSE,
                TraceWriter.AREA_BECKHOFF
              );
              client
                .writeSymbol(symbolDictPut[key], val)
                .then((fn) => {
                  data[symbolDictPut[key]] = Math.round(val * 100) / 100;
                })
                .catch((e) => {
                  traceWriter.trace(
                    e.message,
                    TraceWriter.ERROR,
                    TraceWriter.AREA_BECKHOFF
                  ),
                    database.logs.insert({
                      log: `ERROR: ${e.message}`,
                      createdAt: new Date(),
                    });
                });
            }
          });
        } else {
          res
            .status(400)
            .json({ success: false, message: "val should be a number" });
        }
      }

      if (
        symbol === "set_stirrer" ||
        symbol === "set_cooler" ||
        symbol === "set_lamp" ||
        symbol === "set_roaster" ||
        symbol === "set_abortroast" ||
        symbol === "set_startroast" ||
        symbol === "set_stoproast" ||
        symbol === "reset_alarm_long" ||
        symbol === "reset_alarm" ||
        symbol === "set_destoner" ||
        symbol === "greenbean_loader" ||
        symbol === "green_beans_tare" ||
        symbol === 'set_auto_roast'
      ) {
        if( symbol === "set_abortroast"){
          this.abortingRoast = true;
        }
        if (typeof val === "boolean") {
          Object.keys(symbolDictPut).forEach(function (key) {
            if (key == symbol) {
              traceWriter.trace(
                `Writing Symbol ${symbolDictPut[key]} : ${val}`,
                TraceWriter.VERBOSE,
                TraceWriter.AREA_BECKHOFF
              );
              console.log("symbolDictPut[key], val", symbolDictPut[key], val);
              client
                .writeSymbol(symbolDictPut[key], val)
                .then((fn) => {
                  data[symbolDictPut[key]] = val;
                })
                .catch((e) => {
                  traceWriter.trace(
                    e.message,
                    TraceWriter.ERROR,
                    TraceWriter.AREA_BECKHOFF
                  ),
                    database.logs.insert({
                      log: `ERROR: ${e.message}`,
                      createdAt: new Date(),
                    });
                });
            }
          });
        } else {
          res
            .status(400)
            .json({ success: false, message: "val should be a boolean" });
        }
      }
    }
    return data;
  }

  async readSymbol(symbol, res) {
    let data = {};
    if (simulationMode) {
      let result = simRead(symbol);
      if (typeof result === "number")
        data[symbol] = Math.round(result * 1000) / 1000;
      else data[symbol] = result;
    } else {
      let val;

      if (!this.connected) {
        traceWriter.trace(
          `Not connected`,
          TraceWriter.WARNING,
          TraceWriter.AREA_BECKHOFF
        );
        return {};
      }
      Object.keys(symbolDictGet).forEach(function (key) {
        if (key == symbol) {
          val = symbolDictGet[key];
        }
      });

      // traceWriter.traceInline(`Reading Symbol ${val}`, TraceWriter.INFO, TraceWriter.AREA_BECKHOFF);
      let result = await client.readSymbol(val);
      if (typeof result.value === "number")
        data[symbol] = Math.round(result.value * 1000) / 1000;
      else data[symbol] = result.value;

      // traceWriter.traceInline(`Read Symbol ${val} : ${data}`, TraceWriter.INFO, TraceWriter.AREA_BECKHOFF);
    }
    return data;
  }
}

let beckHoffInstance = null;

module.exports = function beckHoff(roaster) {
  if (!beckHoffInstance) {
    beckHoffInstance = new BeckHoff(roaster);
    beckHoffInstance.init();
  }
  return beckHoffInstance;
};
