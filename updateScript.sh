#!/bin/sh

# Check if script is ran as root
if [[ "$EUID" -ne 0 ]]; then
    echo "This script must be run as root with environment variables! (sudo -E)"
    exit 1
fi

# Check if '-h' or '--help' is provided as parameter and show help
if [[ "$*" == "-h" || "$*" == "--help" ]]
then
    echo "This script must be run as root with environment variables! (sudo -E)"
    echo "Execute this script to run update the backend service of the Dutch Master Roaster."
    echo "Reconfiguration of AmsNetId required"
    exit 1
fi

# Stop the service to update
systemctl stop dmr.service

# Git stuff
eval `ssh-agent -s`
ssh-add ~/.ssh/id_rsa
cd /usr/src/app &&  git checkout master && git pull

# Ask user for TargetAmsNetID and sets the given ID in the config file
echo "Enter LocalAmsNetId: "
read localAmsNetId

echo "Enter TargetAmsNetId: "
read targetAmsNetId

echo "Enter PLCAddress: "
read routerAddress

rm -rf /usr/src/app/config/dmr.js

echo "
const ads = require('ads-client')

const client = new ads.Client({
    localAmsNetId: '$localAmsNetId',  //Can be anything but needs to be in PLC StaticRoutes.xml file
    localAdsPort: 32750,                //Can be anything that is not used
    targetAmsNetId: '$targetAmsNetId',
    targetAdsPort: 851,
    routerAddress: '$routerAddress',     //PLC ip address
    routerTcpPort: 48898                //PLC needs to have this port opened. Test disabling all firewalls if problems
})

module.exports = client; " >> /usr/src/app/config/dmr.js

# Start the service again
systemctl restart dmr.service

