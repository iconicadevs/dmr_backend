var mocks = require('node-mocks-http');
const chai = require('chai')
const expect = chai.expect
const roaster = require('../config/dmr');
const beckHoff = require('../beckhoff')(roaster);

beckHoff.init()

res = mocks.createResponse();

describe("readSymbol()", () => {
    it("Reading symbol that doesn't exist should return 404", async () => {
        await beckHoff.readSymbol("blabla", res)
        expect(res.statusCode).to.equal(404)
    })

    it("Reading symbol lamp returns a bool", async () => {
        let data = await beckHoff.readSymbol("lamp", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.lamp).to.be.an('Boolean')
    })

    it("Reading symbol stirrer returns a bool", async () => {
        let data = await beckHoff.readSymbol("stirrer", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.stirrer).to.be.an('Boolean')
    })

    it("Reading symbol cooler returns a bool", async () => {
        let data = await beckHoff.readSymbol("cooler", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.cooler).to.be.an('Boolean')
    })

    it("Reading symbol roaster returns a string and a Number", async () => {
        let data = await beckHoff.readSymbol("roaster", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.roaster.name).to.be.an('String')
        expect(data.roaster.value).to.be.an('Number')
    })

    it("Reading symbol current_air_temperature returns a Number", async () => {
        let data = await beckHoff.readSymbol("current_air_temperature", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.current_air_temperature).to.be.an('Number')
    })

    it("Reading symbol current_burner_power returns a Number", async () => {
        let data = await beckHoff.readSymbol("current_burner_power", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.current_burner_power).to.be.an('Number')
    })

    it("Reading symbol current_bean_temperature returns a Number", async () => {
        let data = await beckHoff.readSymbol("current_bean_temperature", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.current_bean_temperature).to.be.an('Number')
    })

    it("Reading symbol current_drum_pressure returns a Number", async () => {
        let data = await beckHoff.readSymbol("current_drum_pressure", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.current_drum_pressure).to.be.an('Number')
    })

    it("Reading symbol set_air_temperature returns a Number", async () => {
        let data = await beckHoff.readSymbol("set_air_temperature", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.set_air_temperature).to.be.an('Number')
    })

    it("Reading symbol set_drum_pressure returns a Number", async () => {
        let data = await beckHoff.readSymbol("set_drum_pressure", res)
        expect(data instanceof Object).to.equal(true);
        expect(data.set_drum_pressure).to.be.an('Number')
    })

    
    after(() => { roaster.disconnect();})
}).timeout(10000);