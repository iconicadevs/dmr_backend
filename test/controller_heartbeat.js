const request = require('supertest');
const express = require('express');

const heartbeat = require('../routes/heartbeat');

const app = express();

app.use('/api/v1/heartbeat', heartbeat)

describe('POST /heartbeat', function() {
    it('responds with json', function(done) {
      request(app)
        .post('/api/v1/heartbeat')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)

    }).timeout(10000);
});