const request = require('supertest');
const express = require('express');

const configurations = require('../routes/configurations')
 
const app = express();

app.use('/api/v1/configurations', configurations)

describe('GET /configurations', function() {
    it('responds with json', function(done) {
      request(app)
        .get('/api/v1/configurations')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)
        
    }).timeout(10000);
  })