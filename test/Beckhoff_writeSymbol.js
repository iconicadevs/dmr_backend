var mocks = require('node-mocks-http');
const chai = require('chai')
const expect = chai.expect
const roaster = require('../config/dmr');
const beckHoff = require('../beckhoff')(roaster);

beckHoff.init()

res = mocks.createResponse();

describe("writeSymbol()", () => {
    it("writing wrong symbol should return statusCode 404", async () => {
        await beckHoff.writeSymbol("blabla", 80, res)
        expect(res.statusCode).to.equal(404)
    })

    it("writing non bool value to symbol set_stirrer should return statusCode 400", async () => {
        await beckHoff.writeSymbol("set_stirrer", 80, res)
        expect(res.statusCode).to.equal(400)
    })

    it("writing non bool value to symbol set_cooler should return statusCode 400", async () => {
        await beckHoff.writeSymbol("set_cooler", true, res)
        expect(res.statusCode).to.equal(400)
    })

    it("writing non bool value to symbol set_lamp should return statusCode 400", async () => {
        await beckHoff.writeSymbol("set_lamp", true, res)
        expect(res.statusCode).to.equal(400)
    })

    it("writing non bool value to symbol set_roaster should return statusCode 400", async () => {
        await beckHoff.writeSymbol("set_roaster", true, res)
        expect(res.statusCode).to.equal(400)
    })

    it("writing non number value to set_air_temperature should return statusCode 400", async () => {
        await beckHoff.writeSymbol("set_air_temperature", 120, res)
        expect(res.statusCode).to.equal(400)
    })

    it("writing non number value to set_burner_power should return statusCode 400", async () => {
        await beckHoff.writeSymbol("set_burner_power", 80, res)
        expect(res.statusCode).to.equal(400)
    })

    it("writing non number value to set_drum_pressure should return statusCode 400", async () => {
        await beckHoff.writeSymbol("set_drum_pressure", 80, res)
        expect(res.statusCode).to.equal(400)
    })

    after(() => { roaster.disconnect();  })
}).timeout(10000);