const request = require('supertest');
const express = require('express');

const histories = require('../routes/histories');

const app = express();

app.use('/api/v1/histories', histories)

describe('GET /histories/mostRecentHistory', function() {
    it('responds with json', function(done) {
      request(app)
        .get('/api/v1/histories/mostRecentHistory')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done)

      done()
      process.exit()

    }).timeout(10000);
});