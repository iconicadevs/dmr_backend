const express = require('express');
const ads = require('ads-client');

const client = new ads.Client({
  localAmsNetId: '192.168.1.10.1.2',  //Can be anything but needs to be in PLC StaticRoutes.xml file
  localAdsPort: 32750,                //Can be anything that is not used
  targetAmsNetId: '192.168.86.32.1.1',
  targetAdsPort: 851,
  routerAddress: '192.168.86.32',     //PLC ip address
  routerTcpPort: 48898                //PLC needs to have this port opened. Test disabling all firewalls if problems
})

client.connect()
  .then(res => {   
    //console.log(`Connected to the ${res.targetAmsNetId}`)
    //console.log(`Router assigned us AmsNetId ${res.localAmsNetId} and port ${res.localAdsPort}`)

  client.readSymbol('GVL_VersionInfo.RoasterDescription')
  .then(res => {
    //console.log(`Value read: ${res.value}`)
  })
  .catch(err => {
    //console.log('Something failed:', err)
  })
    
  })
  .then(() => {
    //console.log('Disconnected')
  })
  .catch(err => {
    //console.log('Something failed:', err)
  })
