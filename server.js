require("./config/mongodb");

const express = require("express");
const roaster = require("./config/dmr");
const dotenv = require("dotenv");
const WebSocket = require("ws");
const http = require("http");
const beckHoff = require("./beckhoff")(roaster);
const udpBroadCaster = require("./udpbroadcaster")();
const { handlePrograms } = require("./handlePrograms");
const { traceWriter, TraceWriter } = require("./utils/traceWriter");
const database = require("./config/mongodb").database;

// Load env vars
dotenv.config({ path: "./config/config.env" });

const app = express();
const server = http.createServer(app);
const wss = new WebSocket.Server({ server });

beckHoff.registerFunctionListener(async (JSONdata) => {
  handlePrograms(JSON.parse(JSONdata));
}, "functionClient");

wss.on("connection", async function connection(ws) {
  traceWriter.trace(
    `Client connected`,
    TraceWriter.INFO,
    TraceWriter.AREA_WEBSOCKET
  );
  database.logs.insert({
    log: "Client connected",
    createdAt: new Date(),
  });
  udpBroadCaster.stop();
  beckHoff.registerWebSocketListener(ws, "websocketClient");
  ws.on("close", async function () {
    traceWriter.trace(
      `Client disconnected`,
      TraceWriter.INFO,
      TraceWriter.AREA_WEBSOCKET
    );
    database.logs.insert({
      log: "Client disconnected",
      createdAt: new Date(),
    });
    beckHoff.unregisterListener(ws);
    udpBroadCaster.start();
  });
});

// Body parser
app.use(express.json());

// Route files
const symbols = require("./routes/symbols");
const configurations = require("./routes/configurations");
const histories = require("./routes/histories");
const heartbeat = require("./routes/heartbeat");
const programs = require("./routes/programs");
const logging = require("./routes/logging");

// Mount routers
app.use("/api/v1/symbols", symbols);
app.use("/api/v1/configurations", configurations);
app.use("/api/v1/histories", histories);
app.use("/api/v1/heartbeat", heartbeat);
app.use("/api/v1/programs", programs);
app.use("/api/v1/logging", logging);

server.listen(8080, () => {
  traceWriter.trace(
    "STARTED THE BACKEND ON PORT 8080",
    TraceWriter.INFO,
    TraceWriter.AREA_WEBSOCKET
  );
});
