const roaster = require('../config/dmr')
const beckHoff = require('../beckhoff')(roaster);


currentSec = 0

// @desc        Get all data
// @route       POST /api/v1/heartbeat
exports.sendHeartbeat = async function(req, res) {
    currentSec += 1;

    await beckHoff.writeSymbol("set_heartbeat", currentSec, "")

    if(currentSec >= 1000) {
        currentSec = 0
    }

  res.status(200).json({ success: true })
}


