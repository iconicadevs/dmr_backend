// const database = require('../config/localdb').database;
const asyncHandler = require("../middleware/async");
const ErrorResponse = require("../utils/errorResponse");
const database = require("../config/mongodb").database;
const { traceWriter, TraceWriter } = require("../utils/traceWriter");

// @route       GET /api/v1/histories/getAll
exports.john = asyncHandler(async (req, res, next) => {
  traceWriter.trace(
    "Getting histories",
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );

  let data = await database.historiesCollection.find({}).toArray();

  if (!data) {
    traceWriter.trace(
      "No history found",
      TraceWriter.WARNING,
      TraceWriter.AREA_MONGODB
    );
    return next(new ErrorResponse(`no history`), 404);
  }

  traceWriter.trace(
    "Histories found",
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );
  res.status(200).json({ success: true, data: data });
});
// @desc        update history of a program
// @route       PUT /api/v1/history/:program
// exports.addHistory = asyncHandler(async(req, res, next) => {
//     let data;

//     data = await History.create({
//         history: [
//             {
//                 bean_temperature: 200,
//                 air_temperature: 200,
//                 time: 1
//             },
//             {
//                 bean_temperature: 42,
//                 air_temperature: 34,
//                 time: 2
//             }
//         ]
//     })

//     if(!data) {
//         return next(new ErrorResponse(`failed`), 404);
//     }

//     res.status(200).json({ success: true, data: data })
// })

// @desc        gets the most recent history of the program
// @route       GET /api/v1/histories/mostRecentHistory/:program
exports.getMostRecentHistory = asyncHandler(async (req, res, next) => {
  traceWriter.trace(
    `Getting most recent history for: ${req.params.program}`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );

  //let data = await database.historiesCollection.findOne({ id: Number(req.params.program) });
  let latest;
  if (req.params.program != 0) {
    let data = await database.historiesCollection
      .find({ id: Number(req.params.program) })
      .sort({ date: -1 })
      .toArray();
    latest = data[0];

    if (!latest) {
      traceWriter.trace(
        `No History found on following ID: ${req.params.program}`,
        TraceWriter.WARNING,
        TraceWriter.AREA_MONGODB
      );
      return next(
        new ErrorResponse(
          `No History found on following ID: ${req.params.program}`
        ),
        404
      );
    }
  }

  traceWriter.trace(
    `Most recent history acquired for: ${req.params.program}`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );
  res.status(200).json({ success: true, data: latest });
});
