const roaster = require("../config/dmr");
const beckHoff = require("../beckhoff")(roaster);
const { symbolDictGet } = require("../utils/symbolDicts");
const database = require("../config/mongodb").database;
// const database = require('../config/localdb').database;
const { currentRoast, beanLoader } = require("../handlePrograms");
const { traceWriter, TraceWriter } = require("../utils/traceWriter");

// @desc        Get all data
// @route       GET /api/v1/symbols
exports.getAllData = async function (req, res) {
  let promises = [];

  traceWriter.trace(
    `Getting all data: ${req.params.program}`,
    TraceWriter.INFO,
    TraceWriter.AREA_BECKHOFF
  );

  let symbolDict = { ...symbolDictGet };
  delete symbolDict["events"];

  Object.keys(symbolDict).forEach(function (key) {
    promises.push(beckHoff.readSymbol(key, res));
  });

  const data = await Promise.all(promises);

  traceWriter.trace(
    `Retrieved all data succesfully: ${req.params.program}`,
    TraceWriter.INFO,
    TraceWriter.AREA_BECKHOFF
  );
  res.status(200).json({ success: true, data: data });
};

// @desc        undo symbol
// @route       PUT api/v1/symbols/undo/:symbol
exports.undoSymbol = async function (req, res) {
  let symbol = req.params.symbol;
  let previousOffset = currentRoast.offsets.pop();

  traceWriter.trace(
    `Undoing symbol: ${req.params.symbol}`,
    TraceWriter.INFO,
    TraceWriter.AREA_BECKHOFF
  );
  // console.log("UNDO SYMBOL ", req.params.symbol);

  //("previousoffset", previousOffset.change)

  let currentData = await beckHoff.readSymbol(symbol, res);

  //console.log("currentData", currentData)

  let data = await beckHoff.writeSymbol(
    symbol,
    currentData[symbol] - previousOffset.change,
    res
  );

  //console.log(currentRoast.offsets)

  traceWriter.trace(
    `Symbol undone: ${req.params.symbol}`,
    TraceWriter.INFO,
    TraceWriter.AREA_BECKHOFF
  );
  res.status(200).json({ success: true, data: data });
};

// @desc        Get single symbol
// @route       GET api/v1/symbols/:symbol
exports.getSymbol = async function (req, res) {
  // SYMBOLS
  // lamp
  // stirrer
  // cooler
  // roaster
  // current_air_temperature
  // current_burner_power
  // current_bean_temperature
  // current_drum_pressure
  // set_air_temperature
  // eco_score
  // energy_consumption
  // CO2_reduction

  let symbol = req.params.symbol;

  traceWriter.trace(
    `Getting symbol: ${symbol}`,
    TraceWriter.VERBOSE,
    TraceWriter.AREA_BECKHOFF
  );

  if (symbol === "energy_consumption") {
    let result;
    // database.loadData()
    // result = database.find("energyConsumption",
    //     {
    //         id: 1,
    //     }
    // );
    // database.saveChanges()

    if (database.energyConsumptionCollection != null)
      result = await database.energyConsumptionCollection
        .find({ _id: 1 })
        .toArray();

    if (result) return res.status(200).json({ success: true, data: result });
    else return res.status(200).json({ success: true, data: [] });
  }
  // console.log("*** READING SYMBOL ", symbol);
  const data = await beckHoff.readSymbol(symbol, res);

  if (data != undefined) {
    if (data.roaster != undefined) {
      if (data.roaster.value >= 2 && data.roaster.value <= 7) {
        data.roaster.value = true;
      } else {
        data.roaster.value = false;
      }
    }

    traceWriter.trace(
      `Symbol retrieved succesfully: ${req.params.symbol}`,
      TraceWriter.VERBOSE,
      TraceWriter.AREA_BECKHOFF
    );
    res.status(200).json({ success: true, data: data });
  } else {
    traceWriter.trace(
      `Symbol undefined: ${req.params.symbol}`,
      TraceWriter.WARNING,
      TraceWriter.AREA_BECKHOFF
    );
  }
};

// @desc        Update symbol
// @route       PUT api/v1/symbols/:symbol
exports.updateSymbol = async function (req, res) {
  // Symbols
  // set_air_temperature
  // set_burner_power
  // set_drum_pressure
  // set_stirrer
  // set_cooler
  // set_lamp
  // set_roaster

  let symbol = req.params.symbol;
  //console.log("UPDATE SYMBOL ", symbol);
  let previousValue;

  traceWriter.trace(
    `Updating symbol (${symbol}) with value: ${req.body.val}`,
    TraceWriter.INFO,
    TraceWriter.AREA_BECKHOFF
  );

  let data = await beckHoff.writeSymbol(symbol, req.body.val, res);

  //("symbol", symbol)

  if (symbol === "greenbean_loader") {
    setTimeout(async () => {
      let status_greenbean_loader = await beckHoff.readSymbol(
        "greenbean_loader",
        res
      );
      let bean_weight_green = await beckHoff.readSymbol(
        "bean_weight_green",
        res
      );
      if (status_greenbean_loader.greenbean_loader) {
        // zuiger aangezet
        beanLoader.currentWeight += bean_weight_green.bean_weight_green;
      } else {
        // greenbeanloader uitgezet
        beanLoader.currentWeight -= bean_weight_green.bean_weight_green;
      }

      traceWriter.trace(
        `green bean is nu geregistreed op ${currentRoast.greenBeanWeight}`,
        TraceWriter.INFO,
        TraceWriter.AREA_BECKHOFF
      );
    }, 100);
  }

  if (
    (symbol === "set_drum_pressure" ||
      symbol === "set_air_temperature" ||
      symbol === "set_burner_power") &&
    currentRoast.roasting.roasting
  ) {
    previousValue = await beckHoff.readSymbol(symbol, res);

    if (req.body.val - previousValue[symbol] != 0) {
      const offset = {
        second: Math.round(
          (Date.now() - currentRoast.startRoastTime.startRoastTime) / 1000
        ),
        setting: symbol,
        change: req.body.val - previousValue[symbol],
      };

      traceWriter.trace(
        `Adding offset ${offset}`,
        TraceWriter.INFO,
        TraceWriter.AREA_BECKHOFF
      );

      currentRoast.offsets.push({ done: true, ...offset });
      currentRoast.newProgram.newProgram = true;
    }
  }

  // if((symbol === "set_drum_pressure" || symbol === "set_air_temperature" || symbol === "set_burner_power") && !currentRoast.roasting.roasting) {
  //   switch (symbol) {
  //     case "set_drum_pressure" : currentRoast.presets.drum_pressure =  req.body.val; break;
  //     case "set_air_temperature" : currentRoast.presets.air_temperature =  req.body.val; break;
  //     case "set_burner_power" : currentRoast.presets.burner_power =  req.body.val; break;
  //   }
  // }

  //currentRoast.newProgram.newProgram = true

  //console.log("update symbol offset", currentRoast.offsets)

  traceWriter.trace(
    `Updating symbol (${symbol}) succesful`,
    TraceWriter.INFO,
    TraceWriter.AREA_BECKHOFF
  );
  res.status(200).json({ success: true, data: data });
};
