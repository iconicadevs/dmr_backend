const database = require("../config/mongodb").database;
const asyncHandler = require("../middleware/async");
const ErrorResponse = require("../utils/errorResponse");

const roaster = require("../config/dmr");
const beckHoff = require("../beckhoff")(roaster);
const { currentRoast } = require("../handlePrograms");
const { traceWriter, TraceWriter } = require("../utils/traceWriter");

// @desc        Get all programs
// @route       GET /api/v1/programs
exports.getAllPrograms = asyncHandler(async (req, res, next) => {
  // database.loadData();
  // let programs = database.findAllData("programsCollection", {});
  // database.saveChanges()

  traceWriter.trace(
    `Getting all programs`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );

  let programs = await database.programsCollection.find({}).toArray();

  let data = [];

  programs.forEach((program) => {
    delete program.presets;
    delete program.offsets;

    data.push(program);
  });

  data.sort(function (a, b) {
    return new Date(b.timestamp) - new Date(a.timestamp);
  });

  traceWriter.trace(
    `Acquired all programs succesfully`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );
  res.status(200).json({ success: true, data: data });
});

// @desc        Get single program
// @route       GET /api/v1/programs/:program
exports.getProgram = asyncHandler(async (req, res, next) => {
  // database.loadData();
  // let program = database.find("programsCollection", {id: req.params.program});
  // database.saveChanges()

  traceWriter.trace(
    `Getting the following program: ${req.params.program}`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );

  let program = await database.programsCollection.findOne({
    id: Number(req.params.program),
  });
  // console.log("GET PROGRAM ", program);

  //("program", program)

  if (!program) {
    traceWriter.trace(
      `No program found`,
      TraceWriter.ERROR,
      TraceWriter.AREA_MONGODB
    );
    res.status(404).json({ success: false, message: "Data not available" });
  }

  // delete program.presets
  // delete program.offsets

  traceWriter.trace(
    `Program accquired succesfully`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );
  res.status(200).json({ success: true, data: program });
});

// @desc        renames a single program
// @route       POST /api/v1/programs/:program
exports.renameProgram = asyncHandler(async (req, res, next) => {
  // database.loadData();
  // let result = database.replace("programsCollection", { id: req.params.program }, req.body );
  // database.saveChanges()

  //console.log("data", req.body);
  traceWriter.trace(
    `Trying to rename program: ${req.params.program}`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );

  var myquery = { id: Number(req.params.program) };
  var newvalues = { $set: { ...req.body } };

  let result = await database.programsCollection.updateOne(myquery, newvalues);

  if (!result) {
    traceWriter.trace(
      `Failed to rename program`,
      TraceWriter.ERROR,
      TraceWriter.AREA_MONGODB
    );
    return next(new ErrorResponse(``, 404));
  }

  let program = await database.programsCollection.findOne({
    id: Number(req.params.program),
  });
  if (!program) {
    traceWriter.trace(
      `Failed to find renamed program: ${req.params.program}`,
      TraceWriter.ERROR,
      TraceWriter.AREA_MONGODB
    );
    return next(new ErrorResponse(``, 404));
  }

  traceWriter.trace(
    `Renamed program succesfully`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );
  res.status(200).json({ success: true, data: result });
});

// @desc        sets a program as the currentprogram
// @route       POST /api/v1/programs/set/:program
exports.setProgram = asyncHandler(async (req, res, next) => {
  // database.loadData();
  // let result = database.set("currentProgramCollection", {id: 1}, {id: 1, currentProgram: req.params.program});
  // database.saveChanges()

  //console.log("currentProgram")
  //console.log(req.params.program)
  // console.log("SETTING PROGRAM TO PROGRAM ", req.params.program);

  traceWriter.trace(
    `Setting currentprogram: ${req.params.program}`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );

  await database.currentProgramCollection.remove();
  let result = await database.currentProgramCollection.insert(
    { currentProgram: Number(req.params.program) },
    { upsert: true }
  );
  // set the presets
  let program = await database.programsCollection.findOne({
    id: Number(req.params.program),
  });
  if (program) {
    for (let key in program.presets) {
      traceWriter.trace(
        `Setting the presets`,
        TraceWriter.VERBOSE,
        TraceWriter.AREA_MONGODB
      );

      beckHoff.writeSymbol("set_" + key, program.presets[key]);
    }
  }

  if (!result) {
    traceWriter.trace(
      `This current program does not exist: ${req.params.program}`,
      TraceWriter.ERROR,
      TraceWriter.AREA_MONGODB
    );

    return next(
      new ErrorResponse(
        `This current program does not exist: ${req.params.program}`
      ),
      404
    );
  }

  res.status(200).json({ success: true, data: result });
});

// @desc        gets the current program
// @route       GET /api/v1/programs/currentProgram
exports.currentProgram = asyncHandler(async (req, res, next) => {
  // database.loadData();
  // let result = database.find("currentProgramCollection", {id: 1});
  // database.saveChanges()

  // res.status(200).json({ success: true, data: result })

  //  console.log("GETTING PROGRAM ");\

  traceWriter.trace(
    `Getting the current program`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );

  let result = await database.currentProgramCollection.findOne({});

  // console.log("GETTING PROGRAM ", result);

  // if(!result) {
  //     return next(new ErrorResponse(`No program selected`, 404))
  // }

  traceWriter.trace(
    `Current program acquired`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );

  res.status(200).json({ success: true, data: result });
});

// @desc        set the given crack in memory
// @route       POST/api/v1/programs/currentProgram/setCrack
exports.setCrack = (id, data) => {
  // console.log("setting crack ", id, data);
  if (id > 0 && id < 3) {
    currentRoast.cracks[id - 1] = data;
    traceWriter.trace(
      `Setting crack`,
      TraceWriter.INFO,
      TraceWriter.AREA_MONGODB
    );
    // console.log("currentRoast cracks set to:", currentRoast.cracks);
    currentRoast.saveCracks = true;
  }
};

exports.getProgress = asyncHandler(async (req, res, next) => {
  let hp = require("../handlePrograms");
  res.status(200).json({ success: true, data: hp.history() });
});
