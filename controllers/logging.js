const database = require("../config/mongodb").database;
const asyncHandler = require("../middleware/async");

const { traceWriter, TraceWriter } = require("../utils/traceWriter");

// @desc        Get last log lines
// @route       GET /api/v1/logging
exports.getLogging = asyncHandler(async (req, res, next) => {
  traceWriter.trace(
    `Retrieving last logging lines (48h)`,
    TraceWriter.INFO,
    TraceWriter.AREA_MONGODB
  );

  let logs = await database.logs.find({}).toArray();

  logs.sort(function (a, b) {
    return new Date(b.createdAt) - new Date(a.createdAt);
  });

  json = JSON.stringify({ ...logs });

  res.json(JSON.parse(json));
});
