const asyncHandler = require("../middleware/async");
const client = require("../config/dmr");
const beckHoff = require("../beckhoff")(client);
const { symbolConfigDict } = require("../utils/symbolDicts");
const { traceWriter, TraceWriter } = require('../utils/traceWriter');
const { readSymbol: simRead, writeSymbol: simWrite } = require('../simulation');

let simulationMode = false;
var myArgs = process.argv.slice(2);
if (myArgs.find((i) => i == "--mock")) {
    simulationMode = true;
}

// @desc        Get configuration
// @route       GET api/v1/configurations
exports.getConfiguration = asyncHandler(async (req, res) => {
  if (!beckHoff.connected) {
    traceWriter.trace(
      "Beckhoff not connected!",
      TraceWriter.ERROR,
      TraceWriter.AREA_BECKHOFF
    );
    res.status(404).json({ success: false, data: {} });
  } else {
    let data = {};
    let promises = [];
    let results = [];

    Object.keys(symbolConfigDict).forEach(function (key) {
      if (simulationMode) {
        data[key] = simRead(key);
      } else {
        promises.push(client.readSymbol(symbolConfigDict[key]));
      }
    });

    if (!simulationMode) {
      results = await Promise.all(promises);
   }

    results.forEach((fn) => {
      if (!simulationMode) {
        let key = Object.keys(symbolConfigDict).find(
          (key) => symbolConfigDict[key] === fn.symbol.name
        );
        data[key] = fn.value;
      }
    });

    if (!results) {
      traceWriter.trace(
        "No configuartion info found",
        TraceWriter.WARNING,
        TraceWriter.AREA_BECKHOFF
      );
      res.status(400).json({ success: false });
    }

    res.status(200).json({ success: true, data: data });
  }
});

exports.getGreenBeanInfo = asyncHandler(async (req, res) => {
  if (!beckHoff.connected) {
    traceWriter.trace(
      "Beckhoff not connected!",
      TraceWriter.ERROR,
      TraceWriter.AREA_BECKHOFF
    );
    res.status(404).json({ success: false, data: {} });
  } else {
    let symbols = {
      green_bean_loader: "GVL_Setup_Options.Weighing_Beans_Green",
    };
    let data = {};
    let promises = [];
    let results = [];

    Object.keys(symbols).forEach(function (key) {
      if (simulationMode) {
        data[key] = simRead(key);
      } else {
        promises.push(client.readSymbol(symbols[key]));
      }
    });

    if (!simulationMode) {
       results = await Promise.all(promises);
    }

    results.forEach((fn) => {
      if (!simulationMode) {
        let key = Object.keys(symbols).find(
          (key) => symbols[key] === fn.symbol.name
        );
        data[key] = fn.value;
      }
    });

    if (!results) {
      traceWriter.trace(
        "No Green Bean configuration info found",
        TraceWriter.WARNING,
        TraceWriter.AREA_BECKHOFF
      );
      res.status(400).json({ success: false });
    }
    
    res.status(200).json({ success: true, data: data });
  }
});
