const asyncHandler = require("../middleware/async");

//* MAKE SURE TO UPDATE THIS NUMBER WHEN PUSHING A NEW VERSION
const versionNumber = "3.1.1 (4)";

// @desc        Get version number
// @route       GET api/v1/configurations/version
exports.getVersion = asyncHandler(async (req, res) => {
  res.status(200).json({ version: versionNumber });
});
