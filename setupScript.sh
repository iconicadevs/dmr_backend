#!/bin/sh

# Check if script is ran as root

echo "Make sure script is ran with the -E flag after sudo"

if [[ "$EUID" -ne 0 ]]; then
    echo "This script must be run as root with environment variables! (sudo -E)"
    exit 1
fi

# Check if '-h' or '--help' is provided as parameter and show help
if [[ "$*" == "-h" || "$*" == "--help" ]]
then
    echo "This script must be run as root with environment variables! (sudo -E)"
    echo "Execute this script to initialize and setup all things related to the dutch master roaster"
    echo "This script, changes the password to the given password"
    echo "It setups a static IP-address, installs mongodb, creates the neccessary directories"
    echo "Retrieves the latest version of the backend, creates a service file for the backend and enables it on startup"
    echo "Lastly it reboots te Pi"
    exit 1
fi

# Password stuff
echo "Enter the new password:"
read newPass
echo "pi:$newPass" | chpasswd

# IP stuff
rm -rf /etc/dhcpcd.conf

echo "
interface eth0
static ip_address=192.168.3.159/24
static routers=192.168.3.1
static domain_name_servers=192.168.3.1 8.8.8.8" >> /etc/dhcpcd.conf

# Updating and installing mongod
apt update
apt install mongodb

# Removing a few directories
rm -rf /usr/src/app
rm -rf /home/pi/.ssh
rm -rf /data/db
rm -rf /home/pi/.ssh

# Creating a few directories
mkdir /usr/src/app
mkdir /data
mkdir /data/db
mkdir /home/pi/.ssh

# Adding SSH Key for retrieval access to repo
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDimzyryfon/lnAfgB87qFObLTdJUbR6kvSmq9YWc2Io2viWX0SuRnLVTI7WIoiR1y1um+iw7iwmFGWWqg+2RqpDJr6OAEhglqLO8olvbDhLBFfhG6+ENjgIioeGYT9xNyE2GqxahZiZv7j7gqUHCo5Jn7goT1N755NIEV2QbzXoEQK2+gVLImR0P03l8q1mIk69gnuGiB8DngHOeo3Vy0eLMS6M+jFbvRyFw20/r62gtm+jI+AjnOIXM0ThvZh36lx2XEJQHtgOmgNKHH8ciF3Ok0CvM/87a4YJhrCKYjb3B8jDZ/UvG6eD39Jz6nSQYZ4mGN7AFRu4HS8rhGdKbM2FTQv7IPqEgniukuiCHnnTxW6RyV1xORk9QkEen6FemP0iY+C9y+dO3AmQrd8HfXTqMFIgaO8LSU7PRpb5Gv3BFGC0u4xHxw1fwg5a5zL4y2VZfujIsbsgGAdUl2j1CDVtRqSosx4Txo4kHt2FHkzRCfu8+aNcCtLcf2oGe/aGvE=" >> /home/pi/.ssh/id_rsa.pub
echo "-----BEGIN OPENSSH PRIVATE KEY-----
b3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEbm9uZQAAAAAAAAABAAABlwAAAAdzc2gtcn
NhAAAAAwEAAQAAAYEA4ps8q8n6J/5ZwH4AfO6hTmy03SVG0epL0pqvWFnNiKNr4ll9ErkZ
y1UyO1iKIkdctbpvosO4sJhRllqoPtkaqQya+jgBIYJaizvKJb2w4SwRX4RuvhDY4CIqHh
mE/cTchNhqsWoWYmb+4+4KlBwqOSZ+4KE9Te+eTSBFdkG816BECtvoFSyJkdD9N5fKtZiJ
OvYJ7hogfA54BznqN1ctHizEujPoxW70chcNtP6+toLZvoyPgI5ziFzNE4b2Yd+pcdlxCU
B7YDpoDShx/HIhdzpNArzP/O2uGCYawimI29wfIw2f1Lxung9/Sc+p0kGGeJhjewBUbuB0
vK4RnSmzNhU0L+yD6hIJ4rpLogh5508VukcldcTkZPUJBHp+hXpj9ImPgvcvnTtwJkK3fB
3106jBSIGjvC0lOz0aW+Rr9wRRgtLuMR8cNX8IOWucy+MtlWX7oyLG7IBgHVJdo9Qg1bUa
kqLMeE8aOJB7dhR5M0Qn7vPmjXArS3H9qBnv2hrxAAAFkPCg6YPwoOmDAAAAB3NzaC1yc2
EAAAGBAOKbPKvJ+if+WcB+AHzuoU5stN0lRtHqS9Kar1hZzYija+JZfRK5GctVMjtYiiJH
XLW6b6LDuLCYUZZaqD7ZGqkMmvo4ASGCWos7yiW9sOEsEV+Ebr4Q2OAiKh4ZhP3E3ITYar
FqFmJm/uPuCpQcKjkmfuChPU3vnk0gRXZBvNegRArb6BUsiZHQ/TeXyrWYiTr2Ce4aIHwO
eAc56jdXLR4sxLoz6MVu9HIXDbT+vraC2b6Mj4COc4hczROG9mHfqXHZcQlAe2A6aA0ocf
xyIXc6TQK8z/ztrhgmGsIpiNvcHyMNn9S8bp4Pf0nPqdJBhniYY3sAVG7gdLyuEZ0pszYV
NC/sg+oSCeK6S6IIeedPFbpHJXXE5GT1CQR6foV6Y/SJj4L3L507cCZCt3wd9dOowUiBo7
wtJTs9Glvka/cEUYLS7jEfHDV/CDlrnMvjLZVl+6MixuyAYB1SXaPUINW1GpKizHhPGjiQ
e3YUeTNEJ+7z5o1wK0tx/agZ79oa8QAAAAMBAAEAAAGATD7VXCv6eqhvRIBg46hbKAsVk+
8t9AV0e6lGZGDa8iWicCeSGNGlJzBEVYmF+OCwBglz9H0MUIb/tKpOP89G1CiEoLplTwQQ
ORbl/mrj7++m7M+ff14lysxUBYqqn8Q0OpKi240JeRSUoNaAQrNzVYVpXWWDPgzo46R/Gr
olsxueqVSysyUoKKL8AE+XiiCH1Ani73VlBrjLlArdjr9LVALWMpyyrnOV1imBmCu6uagv
YZjj0+qaXwBylCZuEiJxu65u+6kv00Y8hiz1CIXPyHGohfpsCudk0QLQtX6ath7zwAHXTu
Yf6+Mq1p7z9GA4AhxYSmddt9brtnoKFPxDHCiBLwTp4NTb0mCK/Fql1xY4hO2epNmwr0XJ
SEMQLWSQYIR2O8dhxABoNYfFgZDrkq/U5OBYzovetxWmJZmKt0pmJXbO62p363pkS/aBrI
0g5p1TbR/CUOyOyzg9zTVKJUj66imDoG+5ZnpLV1wh6Bx5je8daSCHlM3wjuY4DXRdAAAA
wQC+Xb1dIw2iuQP6yoXlpM1P1xynpAZFimvmZ7m155vOGvBGdG8ogw2i6Vk2EuVxXhUicH
vittEbN7e5g4TMt4F6SoDH0o/2LHIpwS/6QUoMJH8B3HpoGdRnaE2N8KN2IAcSpe9hqRXG
I4STRK/AVbRZyupkM5H7zrPhcoU8ziGQAVbH4Itu725kTUdHemUaySYl0UO8umyp44p426
Pd+LXSoTX603idO7VozIOu4mFxuUjZXkWQV6Wzg61DnnBmBscAAADBAP1HT3ifYbHR+bUv
+bKgSEf2Rf3ZV7o6EMEdZ49wNTmN5rpubqdQa3iFbJpGweioBtx4f/DmUJFprSmY0egWXf
BJjlq9V+q6O2hxzi3fIzRmlRafu7c/rGVvO75NgRA5LiffhxNBcGlGhVFgP6bNHiyMI/p3
oYZR6S/wwnIeqbJwLllCkRzrh23g5FZp1b3/Tae6YVsBYKxbXO0YHaYKIlDBoX8xXNzqTb
5bQk420T/PVQvdb4v/U2kgG5CfLvoR+wAAAMEA5QqPUdH4cCVOue/pSgfOOOtvotX0xKQh
f0zCAhmUCOnthD7Xc3TrOCvmerbWe58LF/Vez8AuBEFepMpQojTHYIZhv7SlnAEEsOm6wL
rlTQLoLqb9DRPhjfoNbWljkcg7EbgVUe+o/VgGVcjbczYC5w1LfB080O11Vlld1MNULWqd
7BqgsJOHhbsA/wjRLVG31RQxFVNKropR3kOwLgxoZA9ePq7vBXQgBztAK/5JgVqu5JzIPu
ndFMqs4Diac58DAAAAGGpvaG5nb3J0ZXJAam9obnMtbWJwLmxhbgEC
-----END OPENSSH PRIVATE KEY-----" >> /home/pi/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa.pub
eval `ssh-agent -s`
ssh-add ~/.ssh/id_rsa

# Retrieving initial codebase
git clone git@bitbucket.org:iconicadevs/dmr_backend.git /usr/src/app

# Retrieve dependencies
cd /usr/src/app && npm install

# Create service
rm -rf /lib/systemd/system/dmr.service

# Create service
echo "
[Unit]
Description=dmr server
After=network.target
[Service]
Environment=NODE_PORT=8080
Environment=NODE_PORT=3000
Type=simple
User=root
WorkingDirectory=/usr/src/app
ExecStart=/bin/bash autostart.sh
Restart=on-failure
[Install]
WantedBy=multi-user.target" >> /lib/systemd/system/dmr.service

# Enable service on startup
systemctl daemon-reload
systemctl enable dmr.service


# Ask user for DMR Net ID's
echo "Enter LocalAmsNetId: "
read localAmsNetId

echo "Enter TargetAmsNetId: "
read targetAmsNetId

echo "Enter PLCAddress: "
read routerAddress

# Set the given ID in the config file
echo "
const ads = require('ads-client')

const client = new ads.Client({
    localAmsNetId: '$localAmsNetId',  //Can be anything but needs to be in PLC StaticRoutes.xml file
    localAdsPort: 32750,                //Can be anything that is not used
    targetAmsNetId: '$targetAmsNetId',
    targetAdsPort: 851,
    routerAddress: '$routerAddress',     //PLC ip address
    routerTcpPort: 48898                //PLC needs to have this port opened. Test disabling all firewalls if problems
})

module.exports = client; " >> /usr/src/app/config/dmr.js

# Reboot
echo "Script execution succesful! Going to reboot now"
reboot
